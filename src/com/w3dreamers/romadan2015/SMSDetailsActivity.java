package com.w3dreamers.romadan2015;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;

public class SMSDetailsActivity extends Activity{
	
	TextView txtTitle,txtDetails;
	LinearLayout linearShare;
	
	boolean supported;
	Typeface banglaFont;
	
	String details;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.sms_details);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		txtDetails=(TextView)findViewById(R.id.txtDetails);
		linearShare=(LinearLayout)findViewById(R.id.linearShare);
		
		String title="ঈদ এসএমএস";
		
		details=getIntent().getExtras().getString("details");
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		
		txtTitle.setTypeface(banglaFont);
		txtDetails.setTypeface(banglaFont);
		
		if(supported)
		{
			SpannableString convertedTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(title, banglaFont, (float)1);
			txtTitle.setText(convertedTitle);
			SpannableString convertedDetails=AndroidCustomFontSupport.getCorrectedBengaliFormat(details, banglaFont, (float)1);
			txtDetails.setText(convertedDetails);
		}
		else
		{
			txtTitle.setText(title);
			txtDetails.setText(details);
		}
		
		
		linearShare.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
			/*	String uriStr = "sms:";
				Uri smsUri = Uri.parse(uriStr);
				Intent smsIntent = new Intent(Intent.ACTION_VIEW, smsUri);
				smsIntent.putExtra("sms_body",details);
				startActivity(smsIntent);   */
				
				try
				{				
					Intent sendIntent=new Intent();
					sendIntent.setAction(Intent.ACTION_SEND);					
					String link=details;
					sendIntent.putExtra(Intent.EXTRA_TEXT, link);
					sendIntent.setType("text/plain");
					startActivity(sendIntent);					
				}			
				catch(Exception e)
				{
					showToast("দুঃখিত, এসএমএসটি শেয়ার করা সম্ভব হচ্ছে না");
				}
				
			}
		});
		
		
	}
	
	
	
	public void showToast(String text)
	{
		LayoutInflater li = LayoutInflater.from(this);
		View view = li.inflate(R.layout.custom_toast, null);
		TextView txtToast=(TextView)view.findViewById(R.id.txtToast);
		txtToast.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			txtToast.setText(convertedText);
		}
		else
		{
			txtToast.setText(text);
		}
		Toast toast=new Toast(this);
		toast.setView(view);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.show();
	}
	
	

}
