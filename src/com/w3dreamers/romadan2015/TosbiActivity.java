package com.w3dreamers.romadan2015;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TosbiActivity extends Activity{
	
	TextView txtTitle,txtCount,txtClick;
	LinearLayout linearClick,linearReset;
	
	boolean supported;
	Typeface banglaFont;
	
	SharedPreferences prefTosbi;	
	long total;
	final String[] banglaNums={"০","১","২","৩","৪","৫","৬","৭","৮","৯","১০","১১","১২","১৩","১৪","১৫","১৬","১৭","১৮","১৯","২০","২১","২২","২৩","২৪","২৫","২৬","২৭","২৮","২৯","৩০","৩১","৩২","৩৩","৩৪","৩৫","৩৬","৩৭","৩৮","৩৯","৪০","৪১","৪২","৪৩","৪৪","৪৫","৪৬","৪৭","৪৮","৪৯","৫০","৫১","৫২","৫৩","৫৪","৫৫","৫৬","৫৭","৫৮","৫৯","৬০"};
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.tosbi_layout);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		txtCount=(TextView)findViewById(R.id.txtCount);
		txtClick=(TextView)findViewById(R.id.txtClick);
		linearClick=(LinearLayout)findViewById(R.id.linearClick);
		linearReset=(LinearLayout)findViewById(R.id.linearReset);
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		
		txtTitle.setTypeface(banglaFont);
		txtCount.setTypeface(banglaFont);
		txtClick.setTypeface(banglaFont);
		
		String title="তসবীহ্";
		String click="গণনা করতে ক্লিক করুন";
		
		if(supported)
		{
			SpannableString convertedTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(title, banglaFont, (float)1);
			txtTitle.setText(convertedTitle);
			SpannableString convertedClick=AndroidCustomFontSupport.getCorrectedBengaliFormat(click, banglaFont, (float)1);
			txtClick.setText(convertedClick);
		}
		else
		{
			txtTitle.setText(title);
			txtClick.setText(click);
		}
		
		prefTosbi=getSharedPreferences("Tosbi", MODE_PRIVATE);
		total=prefTosbi.getLong("count", 0);
		setCounter();
		
		
		linearClick.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				total++;
				setCounter();
			}
		});
		
		
		linearReset.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				total=0;
				setCounter();
			}
		});
		
		
	}
	
	
	public void setCounter()
	{
		String count="";
		String sTotal=String.valueOf(total);
		for(int i=0;i<sTotal.length();i++)
		{
			String a=""+sTotal.charAt(i);
			int b=Integer.valueOf(a);
			count+=banglaNums[b];
		}
		if(supported)
		{
			SpannableString convertedCount=AndroidCustomFontSupport.getCorrectedBengaliFormat(count, banglaFont, (float)1);
			txtCount.setText(convertedCount);
		}
		else
		{
			txtCount.setText(count);
		}
	}
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		SharedPreferences.Editor tosbiEditor=prefTosbi.edit();
		tosbiEditor.putLong("count", total);
		tosbiEditor.commit();
		super.onBackPressed();
	}
	
	

}
