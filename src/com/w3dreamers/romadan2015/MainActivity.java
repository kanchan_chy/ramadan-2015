package com.w3dreamers.romadan2015;

import java.util.ArrayList;
import java.util.HashMap;

import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

public class MainActivity extends Activity {
	
	SharedPreferences pref;
	SharedPreferences.Editor editor;
	boolean notUpdated=false;
	String value="";
	
	DbHelper dbOpenHelper;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		
		
		pref=getSharedPreferences("database_romadan",0);
		value=pref.getString("load", "no");
		if(value.equals("no"))
		{
			notUpdated=true;
		}
		else
		{
			notUpdated=false;
		}
		
		
		TimeSpender spender=new TimeSpender();
		spender.execute();
		
	}
	
	
	
	class TimeSpender extends AsyncTask<String, String, String>
	{
		
		ArrayList<String>distNames=new ArrayList<String>();
		ArrayList<String>distSeheriTimes=new ArrayList<String>();
		ArrayList<String>distSeheriSigns=new ArrayList<String>();
		ArrayList<String>distIftarTimes=new ArrayList<String>();
		ArrayList<String>distIftarSigns=new ArrayList<String>();
		
		ArrayList<String>dhakaIds=new ArrayList<String>();
		ArrayList<String>dhakaDates=new ArrayList<String>();
		ArrayList<String>dhakaSeHours=new ArrayList<String>();
		ArrayList<String>dhakaSeMinutes=new ArrayList<String>();
		ArrayList<String>dhakaIfHours=new ArrayList<String>();
		ArrayList<String>dhakaIfMinutes=new ArrayList<String>();

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			try
			{
				if(notUpdated)
				{
					dbOpenHelper = new DbHelper(MainActivity.this, Constants.DATABASE_NAME);
					editor=pref.edit();
					editor.putString("load","yes");			
					editor.commit();
				}
				else
				{
					dbOpenHelper = new DbHelper(MainActivity.this, Constants.DATABASE_NAME,1);
				}
			}
			catch(Exception e) {}
			
			try
			{
				Thread.sleep(1500);
			}
			catch(Exception e) {}
			
			try
			{
				HashMap<String,ArrayList<String>> data1	=dbOpenHelper.getAllRow(Constants.TABLE_DISTRICT_TIME, Constants.DISTRICT_TIME_NAME);
				distNames=data1.get(Constants.DISTRICT_TIME_NAME);
				distSeheriTimes=data1.get(Constants.DISTRICT_TIME_TIME_SEHERI);
				distSeheriSigns=data1.get(Constants.DISTRICT_TIME_SIGN_SEHERI);
				distIftarTimes=data1.get(Constants.DISTRICT_TIME_TIME_IFTAR);
				distIftarSigns=data1.get(Constants.DISTRICT_TIME_SIGN_IFTAR);
				
				HashMap<String,ArrayList<String>> data2	=dbOpenHelper.getAllRow(Constants.TABLE_DHAKA_TIME, Constants.DHAKA_TIME_ID);
				dhakaIds=data2.get(Constants.DHAKA_TIME_ID);
				dhakaDates=data2.get(Constants.DHAKA_TIME_DATE);
				dhakaSeHours=data2.get(Constants.DHAKA_TIME_SEHERY_HOUR);
				dhakaSeMinutes=data2.get(Constants.DHAKA_TIME_SEHERY_MINUTE);
				dhakaIfHours=data2.get(Constants.DHAKA_TIME_IFTAR_HOUR);
				dhakaIfMinutes=data2.get(Constants.DHAKA_TIME_IFTAR_MINUTE);
			}
			catch(Exception e) {}
			
			return null;
		}
		
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			Intent in=new Intent(MainActivity.this,OptionActivity.class);
			in.putExtra("notUpdated", notUpdated);
			in.putExtra("distNames", distNames);
			in.putExtra("distSeheriTimes", distSeheriTimes);
			in.putExtra("distSeheriSigns", distSeheriSigns);
			in.putExtra("distIftarTimes", distIftarTimes);
			in.putExtra("distIftarSigns", distIftarSigns);
			in.putExtra("dhakaIds", dhakaIds);
			in.putExtra("dhakaDates", dhakaDates);
			in.putExtra("dhakaSeHours", dhakaSeHours);
			in.putExtra("dhakaSeMinutes", dhakaSeMinutes);
			in.putExtra("dhakaIfHours", dhakaIfHours);
			in.putExtra("dhakaIfMinutes", dhakaIfMinutes);
			startActivity(in);
			finish();
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		}
		
		
	}
	

}
