﻿package com.w3dreamers.romadan2015;

import java.util.ArrayList;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class DoaActivity extends Activity{
	
	TextView txtTitle;
	ListView list;
	
	MyAdapter adapter;
	ArrayList<String>items=new ArrayList<String>();
	
	boolean supported;
	Typeface banglaFont;
	
	String details,header,title;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.doa_layout);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		list=(ListView)findViewById(R.id.listView1);
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		
		txtTitle.setTypeface(banglaFont);
		
		title="দোয়াসমুহ";
		
		if(supported)
		{
			SpannableString convertedTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(title, banglaFont, (float)1);
			txtTitle.setText(convertedTitle);
		}
		else txtTitle.setText(title);
		
		items.add("রোজার নিয়ত");
		items.add("ইফতারের দোয়া");
		items.add("তারাবীহ নামাজের নিয়ত");
		items.add("তারাবীহ নামাজের দোয়া");
		items.add("তারাবীহ নামাজের মোনাজাত");
		items.add("দৈনন্দিন তসবীহ্ ও দোয়া সমুহ");
		
		adapter=new MyAdapter(this, items);
		list.setAdapter(adapter);
		
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				header=items.get(position);
				if(header.equals("দৈনন্দিন তসবীহ্ ও দোয়া সমুহ"))
				{
					Intent in=new Intent(DoaActivity.this,ListActivity.class);
					in.putExtra("title", header);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
				else
				{
					setDetail();
					Intent in=new Intent(DoaActivity.this,DetailsActivity.class);
					in.putExtra("title", title);
					in.putExtra("header", header);
					in.putExtra("details", details);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
			}
		});
		
		
		
	}
	
	
	
	public void setDetail()
	{
		if(header.equals("রোজার নিয়ত"))
		{
			details="বাংলা উচ্চারণঃ\nনাওয়াইতু আন আছুমাগাদাম মিন শাহরি রমাজানাল মুবারাকি ফারদ্বল্লাকা ইয়া আল্লাহু ফাতাকাব্বাল মিন্নি ইন্নীকা আন্তাস সামিউল আলীম।\n\nঅনুবাদঃ\n হে আল্লাহ !! আমি আগামীকাল পবিত্র রমজানের রোজা রাখার নিয়ত করছি, যা তোমার পক্ষ থেকে ফরজ করা হয়েছে। সুতরাং আমার পক্ষ থেকে তা কবুল করো, নিশ্চয়ই তুমি সর্বশ্রোতা ও সর্বজ্ঞ। \n\n";
		}
		else if(header.equals("ইফতারের দোয়া"))
		{
			details="বাংলা উচ্চারণঃ\nআল্লাহুম্মা লাকা ছুমতু ওয়া তাওয়াক্কালতু আলা রিজক্কিকা আফতারতু বি-রহমাতিকা ইয়া আরহামার রহিমীন।\n\nঅনুবাদঃ\nহে আল্লাহ !! আমি তোমার জন্য রোজা রেখেছি এবং তোমার রিজিক দ্বারা ইফতার করছি।\n\n";
		}
		else if(header.equals("তারাবীহ নামাজের নিয়ত"))
		{
			details="শুধু মাত্র রমজান মাসে এই তারাবীহ এর নামাজ পড়তে হয়। এশার নামাজের ২ রাকাত সুন্নত আদায় করার পরে এবং বিতর নামাজ এর আগে ২০ রাকাত তারাবীহ নামাজ আদায় করতে হয়। \n\nতারাবীহ নামাজের নিয়ত:\nবাংলা উচ্চারণঃ\nনাওয়াইতু আন উসাল্লিয়া লিল্লা-হি তা আলা রাকয়াতাই সিলাতিৎ তারাবীহী সুন্নাতু রাসূলিল্লা-হি তাআলা মুতাওয়াজিহান ইলা জিহাতিল কাবাতিশ শারীফাতি, আল্লাহু আকবার।\n\nঅনুবাদঃ\nআমি আল্লাহতাআলার সন্তুষ্টির জন্য রাসূলের সুন্নাত দুইরাকাত তারাবীহ নামায পবিত্র ক্কাবা শরীফের দিকে মুখ করে আদায় করার জন্য নিয়ত করছি ।  আল্লাহু আকবার ।\n\n";
		}
		else if(header.equals("তারাবীহ নামাজের দোয়া"))
		{
			details="প্রত্যেক চার রাকায়াত নামায পড়বার পর নিম্নলিখিত দোয়া তিনবার পড়িবে:\n\nবাংলা উচ্চারণঃ\nসুব্হানাযিল মুলকি ওয়াল মালাকুতি সুবহানাযিল ইযযাতি ওয়াল আযমাতি ওয়াল হাইবাতি ওয়াল কুদরাতি ওয়াল কিবরিয়ায়ি ওয়াল জাবারূত। সুব্হানাল মালিকিল হায়্যিল্লাযি লা-ইয়ানামু ওয়ালা ইয়ামুতু আবাদান আবাদা। সুব্বুহুন কুদ্দুছুন রাব্বুনা ওয়া রাব্বুল মালাইকাতি ওয়ার রূহ।\n\nঅনুবাদঃ\nঅনুবাদঃ আমি তাঁর পবিত্রতা ঘোষণা করছি যিনি রাজধিরাজ এবং ফেরেশতাদের অধিকর্তা । আমি তাঁর পবিত্রতা ঘোষণা করছি যিনি সকল মান-সম্মানের অধিকারী এবং সর্বমহান এবং বিরাট ভয় উৎপাদনকারী এবং অসীম ক্ষমতাশালী গৌরবান্বিত ও শ্রেষ্ঠতম, আমি তাঁরই পবিত্রতা ঘোষণা করি যিনি চিরঞ্জীব বাদশাহ যিনি না নিদ্রা যান আর না তন্দ্রা তাঁকে স্পর্শ করতে পারে । তিনিই আমাদের পালনকর্তা এবং ফেরেশতা ও আত্মাসমূহের রক্ষাকর্তা । \n\n";
		}
		else if(header.equals("তারাবীহ নামাজের মোনাজাত"))
		{
			details="প্রত্যেক চার রাকয়াত নামাযের পর এই মোনাজাত পড়তে হবে:\n\nবাংলা উচ্চারণঃ\nআল্লা-হুম্মা ইন্না নাস আলুকাল্ জান্নাতা ওয়া নাউজুবিকা মিনান্নারি ইয়া খালিকাল জান্নাতা ওয়ান্নারি বিরাহমাতিকা ইয়া আজীজু, ইয়া গাফ্ফারু, ইয়া কারীমু, ইয়া সাত্তারু, ইয়া রাহিমু ,ইয়া জাব্বারু ইয়া খালেকু, ইয়া রাররূ, আল্লাহুমা আজির না মিনান্নারি, ইয়া মূজিরু ইয়া মুজিরু, বিরাহ্মাতিকা ইয়া আরহামার রাহিমীন।\n\nঅনুবাদঃ\nহে আল্লাহ! নিশ্চয়ই আমরা আপনার নিকট হতে বেহেশ্ত কামনা করছি এবং দোযখ হতে পরিত্রাণ পাওয়ার আশা করছি । হে জান্নাত ও জাহান্নামের স্রষ্টা ! আপনি অনুগ্রহ করুন । হে পরাক্রমশালী সর্বজয়ী, হে(দোষ ত্রুটি) গোপনকারী, হে মহান দয়ালু, হে মহান শক্তিশালী, হে মহান সৃষ্টিকর্তা, হে মহান উপকারী, হে আল্লাহ! আমাদের জাহান্নাম হতে রক্ষা করুন। হে রক্ষাকারী! হে রক্ষাকারী! হে রক্ষাকারী! হে পরম দাতা ও দয়ালু! (আল্লাহ) আপনার করুণায় আমাদেরকে মুক্তিদান করুন ।\n\n";
		}
	}
	
	

}
