package com.w3dreamers.romadan2015;

import java.io.IOException;
import java.util.Calendar;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Vibrator;
import android.provider.Settings;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AlarmPopUp extends Activity
{
    private static final int DIALOG_ALARM = 0;
    
   // Ringtone ringtone;  
    MediaPlayer mMediaPlayer;
    Uri uri;
    
    Vibrator vibe;
    
    boolean supported,vibStatus,mediaStatus;
	Typeface banglaFont;
	
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);   
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup);
        
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
        
		
		SharedPreferences vibPref=getSharedPreferences("Vivration", MODE_PRIVATE);
		vibStatus=vibPref.getBoolean("vibrate", false);
		
		SharedPreferences ringPref=getSharedPreferences("Ringtone", MODE_PRIVATE);
        String ring=ringPref.getString("ringtone","");
        if(ring.equals(""))
        {
        	uri=RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        }
        else
        {
        	uri=Uri.parse(ring);
        }
     //   ringtone = RingtoneManager.getRingtone(getApplicationContext(), uri);
     //   ringtone.play();
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        if(audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) 
        {
            try {
            	mMediaPlayer = new MediaPlayer();
                mMediaPlayer.setDataSource(this,uri);
            	mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mMediaPlayer.setLooping(true);
				mMediaPlayer.prepare();
				mMediaPlayer.start();
				mediaStatus=true;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        else mediaStatus=false;
        
       if(vibStatus)
       {
    	   vibe = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
    	   long[] pattern = { 0, 200, 0 };
           vibe.vibrate(pattern,0);
       }
       
       showAlarmDialog();
       
       
    }
    
    
    public void showAlarmDialog()
    {
    	
    	LayoutInflater inflater=LayoutInflater.from(AlarmPopUp.this);
		View view=inflater.inflate(R.layout.warning_dialog, null);
		LinearLayout linearClose;
		TextView txtClose,txtDialogTitle,txtDialogBody;
		linearClose=(LinearLayout)view.findViewById(R.id.linearClose);
		txtClose=(TextView)view.findViewById(R.id.txtClose);
		txtDialogTitle=(TextView)view.findViewById(R.id.txtDialogTitle);
		txtDialogBody=(TextView)view.findViewById(R.id.txtDialogBody);
		Typeface banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		
		txtDialogBody.setTypeface(banglaFont);
		txtDialogTitle.setTypeface(banglaFont);
		txtClose.setTypeface(banglaFont);
		
		String header="পবিত্র মাহে রমজান ২০১৫";
		String close="বন্ধ করুন";
		String warning="";
		
		Calendar cal=Calendar.getInstance();
		int hour=cal.get(Calendar.HOUR_OF_DAY);
		
		if(hour>=12)
		{
			warning="আপনি আজকের ইফতারের জন্য তৈরি তো? আজকের ইফতারের জন্য নিজেকে তৈরি করুন।";
		}
		else
		{
			warning="আপনি আজকের সেহরীর জন্য তৈরি তো? আজকের সেহরীর জন্য নিজেকে তৈরি করুন।";
		}
		
		if(supported)
		{
			SpannableString convertedHeader=AndroidCustomFontSupport.getCorrectedBengaliFormat(header, banglaFont, (float)1);
			txtDialogTitle.setText(convertedHeader);
			SpannableString convertedWarning=AndroidCustomFontSupport.getCorrectedBengaliFormat(warning, banglaFont, (float)1);
			txtDialogBody.setText(convertedWarning);
			SpannableString convertedClose=AndroidCustomFontSupport.getCorrectedBengaliFormat(close, banglaFont, (float)1);
			txtClose.setText(convertedClose);
		}
		else
		{
			txtDialogTitle.setText(header);
			txtDialogBody.setText(warning);
			txtClose.setText(close);
		}
		
		AlertDialog.Builder builder=new AlertDialog.Builder(AlarmPopUp.this);
		builder.setView(view);
		builder.setCancelable(false);
		
	    final AlertDialog dialog=builder.create();
		dialog.show();
		
		linearClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub	
				if(vibStatus) vibe.cancel();
				if(mediaStatus)
				{
					mMediaPlayer.pause();
					mMediaPlayer.stop();
				}
				dialog.cancel();
				AlarmPopUp.this.finish();
			}
		});
    	
    }
        
        
        
 /*
   @Override
    protected Dialog onCreateDialog(int id)
    {
        super.onCreateDialog(id);
 
        // Build the dialog
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
 
        alert.setTitle("MEDIKIT");
        alert.setMessage("Its time to ");
        alert.setCancelable(false);
 
        alert.setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            	// getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
            	ringtone.stop();          	
                AlarmPopUp.this.finish();
            }
        });
        AlertDialog dlg = alert.create();
        return dlg;
    }  */
   
}