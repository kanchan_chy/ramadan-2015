package com.w3dreamers.romadan2015;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.w3dreamers.db.Constants;
import com.w3dreamers.db.DbHelper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.DigitalClock;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class OptionActivity extends Activity  implements LocationListener,GooglePlayServicesClient.ConnectionCallbacks,GooglePlayServicesClient.OnConnectionFailedListener{
	
	Spinner spinner1;
	TextView txtDate,txtRemaining,txtSeheri,txtIftar,txtSchedule,txtAlarm,txtMosque,txtDoa,txtRoja,txtTosbi,txtSMS,txtAfter,txtNirmata;
	LinearLayout linearAfter,linearBefore,linearNirmata;
	HorizontalScrollView scroll;
	
	SpinAdapter adapter;
	
	public static boolean clicked=false;
	public static int regionPos;
	String region;
	int rojaSerial;
	
	boolean supported;
	SharedPreferences prefSupport;
	Typeface banglaFont;
	
	LocationClient locationClient;
	int count;
	
	AlertDialog progress;
	
	double mLat,mLon;
	
	String API_KEY="AIzaSyAPrOxAoTKUdaXtktg4B2QrdPZO5SpM0VQ";
	
	AlertDialog dialog;
	
	String date,remaining,seheri,iftar,schedule,alarm,mosque,doa,roja,tosbi,sms,nirmata,afterText;
	DbHelper dbOpenHelper;
	boolean notUpdated;
	
	int currentDay,currentMonth,currentYear;
	
	final String[] banglaMonths = {"","জানুয়ারি", "ফেব্রুয়ারি", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টোবর", "নভেম্বর", "ডিসেম্বর"};
	final String[] banglaNums={"০","১","২","৩","৪","৫","৬","৭","৮","৯","১০","১১","১২","১৩","১৪","১৫","১৬","১৭","১৮","১৯","২০","২১","২২","২৩","২৪","২৫","২৬","২৭","২৮","২৯","৩০","৩১","৩২","৩৩","৩৪","৩৫","৩৬","৩৭","৩৮","৩৯","৪০","৪১","৪২","৪৩","৪৪","৪৫","৪৬","৪৭","৪৮","৪৯","৫০","৫১","৫২","৫৩","৫৪","৫৫","৫৬","৫৭","৫৮","৫৯","৬০"};
	final String[] prefixes={"","১ম","২য়","৩য়","৪র্থ","৫ম","৬ষ্ঠ","৭ম","৮ম","৯ম","১০ম"};
	
	ArrayList<String>distNames=new ArrayList<String>();
	ArrayList<String>distSeheriTimes=new ArrayList<String>();
	ArrayList<String>distSeheriSigns=new ArrayList<String>();
	ArrayList<String>distIftarTimes=new ArrayList<String>();
	ArrayList<String>distIftarSigns=new ArrayList<String>();
	
	ArrayList<String>dhakaIds=new ArrayList<String>();
	ArrayList<String>dhakaDates=new ArrayList<String>();
	ArrayList<String>dhakaSeHours=new ArrayList<String>();
	ArrayList<String>dhakaSeMinutes=new ArrayList<String>();
	ArrayList<String>dhakaIfHours=new ArrayList<String>();
	ArrayList<String>dhakaIfMinutes=new ArrayList<String>();
	
	ArrayList<String>rojaDays=new ArrayList<String>();
	ArrayList<String>rojaMonths=new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.option_layout);		
		spinner1=(Spinner)findViewById(R.id.spinner1);
		linearAfter=(LinearLayout)findViewById(R.id.linearAfter);
		linearBefore=(LinearLayout)findViewById(R.id.linearBefore);
		linearNirmata=(LinearLayout)findViewById(R.id.linearNirmata);
		txtSchedule=(TextView)findViewById(R.id.txtSchedule);
		txtAlarm=(TextView)findViewById(R.id.txtAlarm);
		txtMosque=(TextView)findViewById(R.id.txtMosque);
		txtDoa=(TextView)findViewById(R.id.txtDoa);
		txtRoja=(TextView)findViewById(R.id.txtRoja);
		txtTosbi=(TextView)findViewById(R.id.txtTosbi);
		txtSMS=(TextView)findViewById(R.id.txtSMS);
		txtDate=(TextView)findViewById(R.id.txtDate);
		txtRemaining=(TextView)findViewById(R.id.txtRemaining);
		txtSeheri=(TextView)findViewById(R.id.txtSeheri);
		txtIftar=(TextView)findViewById(R.id.txtIftar);
		txtAfter=(TextView)findViewById(R.id.txtAfter);
		txtNirmata=(TextView)findViewById(R.id.txtNirmata);
		scroll=(HorizontalScrollView)findViewById(R.id.horizontalScrollView1);
		
		
		prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		linearAfter.setVisibility(View.INVISIBLE);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		
		txtSchedule.setTypeface(banglaFont);
		txtAlarm.setTypeface(banglaFont);
		txtMosque.setTypeface(banglaFont);
		txtDoa.setTypeface(banglaFont);
		txtRoja.setTypeface(banglaFont);
		txtTosbi.setTypeface(banglaFont);
		txtSMS.setTypeface(banglaFont);
		txtDate.setTypeface(banglaFont);
		txtRemaining.setTypeface(banglaFont);
		txtSeheri.setTypeface(banglaFont);
		txtIftar.setTypeface(banglaFont);
		txtNirmata.setTypeface(banglaFont);
		
		
		schedule="রোজার সময়সূচী";
		alarm="অ্যালার্ম";
		mosque="মসজিদ";
		doa="দোয়াসমূহ";
		roja="রোজার তথ্য";
		tosbi="তসবীহ্";
		sms="ঈদ এসএমএস";
		nirmata="অ্যাপ নির্মাতা";
		afterText="আল্লাহ্‌ পরম করুণাময়\nধর্মের পথে চলুন";
		
		
		notUpdated=getIntent().getExtras().getBoolean("notUpdated");
		distNames=(ArrayList<String>)getIntent().getSerializableExtra("distNames");
		distSeheriTimes=(ArrayList<String>)getIntent().getSerializableExtra("distSeheriTimes");
		distSeheriSigns=(ArrayList<String>)getIntent().getSerializableExtra("distSeheriSigns");
		distIftarTimes=(ArrayList<String>)getIntent().getSerializableExtra("distIftarTimes");
		distIftarSigns=(ArrayList<String>)getIntent().getSerializableExtra("distIftarSigns");
		dhakaIds=(ArrayList<String>)getIntent().getSerializableExtra("dhakaIds");
		dhakaDates=(ArrayList<String>)getIntent().getSerializableExtra("dhakaDates");
		dhakaSeHours=(ArrayList<String>)getIntent().getSerializableExtra("dhakaSeHours");
		dhakaSeMinutes=(ArrayList<String>)getIntent().getSerializableExtra("dhakaSeMinutes");
		dhakaIfHours=(ArrayList<String>)getIntent().getSerializableExtra("dhakaIfHours");
		dhakaIfMinutes=(ArrayList<String>)getIntent().getSerializableExtra("dhakaIfMinutes");
		
		try
		{
			dbOpenHelper = new DbHelper(OptionActivity.this, Constants.DATABASE_NAME,1);
		}
		catch(Exception e) {}
		
		if(notUpdated)
		{
			setLibrary();
		}
		else
		{
			setData();
		}				
		
		
		
		spinner1.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				clicked=true;
				region=distNames.get(position);
				SharedPreferences prefRegion=getSharedPreferences("SelectedRegion", MODE_PRIVATE);
				SharedPreferences.Editor regionEditor=prefRegion.edit();
				regionEditor.putString("region", region);
				regionEditor.commit();
				setSeheryTime();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
	}
	
	
	public void setLibrary()
	{
		
		LayoutInflater inflater=LayoutInflater.from(OptionActivity.this);
		View view=inflater.inflate(R.layout.warning_dialog, null);
		LinearLayout linearYes,linearNo;
		TextView txtYes,txtNo,txtDialogTitle,txtDialogBody;
		linearYes=(LinearLayout)view.findViewById(R.id.linearYes);
		linearNo=(LinearLayout)view.findViewById(R.id.linearNo);
		txtYes=(TextView)view.findViewById(R.id.txtYes);
		txtNo=(TextView)view.findViewById(R.id.txtNo);
		txtDialogBody=(TextView)view.findViewById(R.id.txtDialogBody);
		Typeface banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		txtDialogBody.setTypeface(banglaFont);
		
		String warning="আপনার মোবাইল ফোনে কি বাংলা ফন্ট ঠিকমত পড়া যাচ্ছে? যদি পড়া যাই তাহলে ইয়েস বাটনে প্রেস করুন অন্যথায় নো বাটনে প্রেস করুন।";
		SpannableString convertedWarning=AndroidCustomFontSupport.getCorrectedBengaliFormat(warning, banglaFont, (float)1);
		txtDialogBody.setText(convertedWarning);
		
		AlertDialog.Builder builder=new AlertDialog.Builder(OptionActivity.this);
		builder.setView(view);
		builder.setCancelable(false);
		
	    dialog=builder.create();
		dialog.show();
		
		linearYes.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				SharedPreferences.Editor editorSupport=prefSupport.edit();
				editorSupport.putBoolean("supported", true);
				editorSupport.commit();
				
				supported=true;
				setData();			
			}
		});
		
		
		linearNo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				SharedPreferences.Editor editorSupport=prefSupport.edit();
				editorSupport.putBoolean("supported", false);
				editorSupport.commit();
				
				supported=false;
				setData();
			}
		});
		
	}
	
	
	public void setData()
	{
		SharedPreferences prefRegion=getSharedPreferences("SelectedRegion", MODE_PRIVATE);
		region=prefRegion.getString("region", "ঢাকা");
		
		Calendar calender=Calendar.getInstance();
		currentDay=calender.get(Calendar.DAY_OF_MONTH);
		currentMonth=calender.get(Calendar.MONTH)+1;
	    currentYear=calender.get(Calendar.YEAR);
		
		rojaSerial=-1;
		for(int i=0;i<dhakaIds.size();i++)
		{
			String dDate=dhakaDates.get(i);
			int state=0;
			int day=0,month=0,year=0;
			for(int j=0;j<dDate.length();j++)
			{
				if(dDate.charAt(j)=='.')
				{
					state++;
				}
				else
				{
					if(state==0) day=day*10+(dDate.charAt(j)-48);
					else if(state==1) month=month*10+(dDate.charAt(j)-48);
					else year=year*10+(dDate.charAt(j)-48);
				}
			}
			rojaDays.add(String.valueOf(day));
			rojaMonths.add(String.valueOf(month));
			if(day==currentDay&&month==currentMonth&&year==currentYear)
			{
				rojaSerial=Integer.valueOf(dhakaIds.get(i));
			}
		}
		if(rojaSerial>=1&&rojaSerial<=10) remaining=prefixes[rojaSerial]+" রোজা";
		else if(rojaSerial>=11&&rojaSerial<=30) remaining=banglaNums[rojaSerial]+"তম রোজা";
		else txtRemaining.setVisibility(View.INVISIBLE);
		
		date=banglaMonths[currentMonth]+" "+banglaNums[currentDay]+", ";
		String sYear=String.valueOf(currentYear);
		for(int i=0;i<sYear.length();i++)
		{
			String a=""+sYear.charAt(i);
			int b=Integer.valueOf(a);
			date+=banglaNums[b];
		}
		
		if(supported)
		{
			SpannableString convertedSchedule=AndroidCustomFontSupport.getCorrectedBengaliFormat(schedule, banglaFont, (float)1);
			SpannableString convertedAlarm=AndroidCustomFontSupport.getCorrectedBengaliFormat(alarm, banglaFont, (float)1);
			SpannableString convertedMosque=AndroidCustomFontSupport.getCorrectedBengaliFormat(mosque, banglaFont, (float)1);
			SpannableString convertedDoa=AndroidCustomFontSupport.getCorrectedBengaliFormat(doa, banglaFont, (float)1);
			SpannableString convertedRoja=AndroidCustomFontSupport.getCorrectedBengaliFormat(roja, banglaFont, (float)1);
			SpannableString convertedTosbi=AndroidCustomFontSupport.getCorrectedBengaliFormat(tosbi, banglaFont, (float)1);
			SpannableString convertedSMS=AndroidCustomFontSupport.getCorrectedBengaliFormat(sms, banglaFont, (float)1);
			SpannableString convertedDate=AndroidCustomFontSupport.getCorrectedBengaliFormat(date, banglaFont, (float)1);
			SpannableString convertedNirmata=AndroidCustomFontSupport.getCorrectedBengaliFormat(nirmata, banglaFont, (float)1);
			txtSchedule.setText(convertedSchedule);
			txtAlarm.setText(convertedAlarm);
			txtMosque.setText(convertedMosque);
			txtDoa.setText(convertedDoa);
			txtRoja.setText(convertedRoja);
			txtTosbi.setText(convertedTosbi);
			txtSMS.setText(convertedSMS);
			txtDate.setText(convertedDate);
			txtNirmata.setText(convertedNirmata);
			if(rojaSerial!=-1)
			{
				SpannableString convertedRemaining=AndroidCustomFontSupport.getCorrectedBengaliFormat(remaining, banglaFont, (float)1);
				txtRemaining.setText(convertedRemaining);
			}
		}
		else
		{
			txtSchedule.setText(schedule);
			txtAlarm.setText(alarm);
			txtMosque.setText(mosque);
			txtDoa.setText(doa);
			txtRoja.setText(roja);
			txtTosbi.setText(tosbi);
			txtSMS.setText(sms);
			txtDate.setText(date);
			txtNirmata.setText(nirmata);
			if(rojaSerial!=-1)
			{
				txtRemaining.setText(remaining);
			}
		}
		
		if(rojaSerial>=1&&rojaSerial<=29)
		{
			txtRemaining.setVisibility(View.VISIBLE);
			linearAfter.setVisibility(View.INVISIBLE);
			linearBefore.setVisibility(View.VISIBLE);
			
			setSeheryTime();
			
			adapter=new SpinAdapter(this, distNames,1);
			spinner1.setAdapter(adapter);
			spinner1.setSelection(regionPos);
		}
		else
		{
			txtRemaining.setVisibility(View.INVISIBLE);
			linearBefore.setVisibility(View.INVISIBLE);
			linearAfter.setVisibility(View.VISIBLE);
			
			txtAfter.setTypeface(banglaFont);
			String after;
			if((currentDay>=17&&currentDay<=19)&&currentMonth==7&&currentYear==2015)
			{
				after="ঈদ মোবারক\nআপনার ঈদ হোক আনন্দময়";
			}
			else
			{
				after=afterText;
			}
			if(supported)
			{
				SpannableString convertedAfter=AndroidCustomFontSupport.getCorrectedBengaliFormat(after, banglaFont, (float)1);
				txtAfter.setText(convertedAfter);
			}
			else txtAfter.setText(after);
		}
		if(notUpdated) dialog.dismiss();
		
	}
	
	
	public void setSeheryTime()
	{
		String time1Seheri="0",signSeheri="+",time1Iftar="0",signIftar="+";
		for(int i=0;i<distNames.size();i++)
		{
			if(distNames.get(i).equals(region))
			{
				regionPos=i;
				time1Seheri=distSeheriTimes.get(i);
				signSeheri=distSeheriSigns.get(i);
				time1Iftar=distIftarTimes.get(i);
				signIftar=distIftarSigns.get(i);
				break;
			}
		}
		int seHour=0,seMinute=0,ifHour=0,ifMinute=0;
		for(int i=0;i<dhakaIds.size();i++)
		{
			if(dhakaIds.get(i).equals(rojaSerial+""))
			{
				seHour=Integer.valueOf(dhakaSeHours.get(i));
				seMinute=Integer.valueOf(dhakaSeMinutes.get(i));
				ifHour=Integer.valueOf(dhakaIfHours.get(i));
				ifMinute=Integer.valueOf(dhakaIfMinutes.get(i));
				break;
			}
		}
		int timeSeheri=Integer.valueOf(time1Seheri);
		int timeIftar=Integer.valueOf(time1Iftar);
		if(signSeheri.equals("+"))
		{
			seMinute=seMinute+timeSeheri;
			if(seMinute>59)
			{
				seMinute=seMinute-60;
				seHour=seHour+1;
			}
		}
		else
		{
			seMinute=seMinute-timeSeheri;
			if(seMinute<0)
			{
				seMinute=60+seMinute;
				seHour=seHour-1;
			}
		}
		if(signIftar.equals("+"))
		{
			ifMinute=ifMinute+timeIftar;
			if(ifMinute>59)
			{
				ifMinute=ifMinute-60;
				ifHour=ifHour+1;
			}
		}
		else
		{
			ifMinute=ifMinute-timeIftar;
			if(ifMinute<0)
			{
				ifMinute=60+ifMinute;
				ifHour=ifHour-1;
			}
		}
		seheri="সেহরীঃ ভোর "+banglaNums[seHour]+"টা  "+banglaNums[seMinute]+"মি.";
		iftar="ইফতারঃ সন্ধ্যা "+banglaNums[ifHour]+"টা  "+banglaNums[ifMinute]+"মি.";
		if(supported)
		{
			SpannableString convertedSeheri=AndroidCustomFontSupport.getCorrectedBengaliFormat(seheri, banglaFont, (float)1);
			SpannableString convertedIftar=AndroidCustomFontSupport.getCorrectedBengaliFormat(iftar, banglaFont, (float)1);
			txtSeheri.setText(convertedSeheri);
			txtIftar.setText(convertedIftar);
		}
		else
		{
			txtSeheri.setText(seheri);
			txtIftar.setText(iftar);
		}
	}
	
	
	public void optionSelected(View view)
	{
		if(view.getId()==R.id.linearSchedule)
		{
			scroll.scrollTo(0, 0);
			ArrayList<String>seHours=new ArrayList<String>();
			ArrayList<String>seMinutes=new ArrayList<String>();
			ArrayList<String>ifHours=new ArrayList<String>();
			ArrayList<String>ifMinutes=new ArrayList<String>();
			
			String time1Seheri="0",signSeheri="+",time1Iftar="0",signIftar="+";
			for(int i=0;i<distNames.size();i++)
			{
				if(distNames.get(i).equals(region))
				{
					regionPos=i;
					time1Seheri=distSeheriTimes.get(i);
					signSeheri=distSeheriSigns.get(i);
					time1Iftar=distIftarTimes.get(i);
					signIftar=distIftarSigns.get(i);
					break;
				}
			}
			int timeSeheri=Integer.valueOf(time1Seheri);
			int timeIftar=Integer.valueOf(time1Iftar);
			int seMinute=0,seHour=0,ifMinute=0,ifHour=0;
			for(int i=0;i<dhakaIds.size();i++)
			{
				seHour=Integer.valueOf(dhakaSeHours.get(i));
				seMinute=Integer.valueOf(dhakaSeMinutes.get(i));
				ifHour=Integer.valueOf(dhakaIfHours.get(i));
				ifMinute=Integer.valueOf(dhakaIfMinutes.get(i));
				
				if(signSeheri.equals("+"))
				{
					seMinute=seMinute+timeSeheri;
					if(seMinute>59)
					{
						seMinute=seMinute-60;
						seHour=seHour+1;
					}
				}
				else
				{
					seMinute=seMinute-timeSeheri;
					if(seMinute<0)
					{
						seMinute=60+seMinute;
						seHour=seHour-1;
					}
				}
				if(signIftar.equals("+"))
				{
					ifMinute=ifMinute+timeIftar;
					if(ifMinute>59)
					{
						ifMinute=ifMinute-60;
						ifHour=ifHour+1;
					}
				}
				else
				{
					ifMinute=ifMinute-timeIftar;
					if(ifMinute<0)
					{
						ifMinute=60+ifMinute;
						ifHour=ifHour-1;
					}
				}
				seHours.add(String.valueOf(seHour));
				seMinutes.add(String.valueOf(seMinute));
				ifHours.add(String.valueOf(ifHour));
				ifMinutes.add(String.valueOf(ifMinute));
				
			}
			
			Intent in=new Intent(OptionActivity.this,ScheduleActivity.class);
			in.putExtra("ids", dhakaIds);
			in.putExtra("rojaDays", rojaDays);
			in.putExtra("rojaMonths", rojaMonths);
			in.putExtra("seHours", seHours);
			in.putExtra("seMinutes", seMinutes);
			in.putExtra("ifHours", ifHours);
			in.putExtra("ifMinutes", ifMinutes);
			startActivity(in);
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		}
		else if(view.getId()==R.id.linearAlarm)
		{
			scroll.scrollTo((scroll.getRight()/8)*3, 0);
			ArrayList<String>seHours=new ArrayList<String>();
			ArrayList<String>seMinutes=new ArrayList<String>();
			ArrayList<String>ifHours=new ArrayList<String>();
			ArrayList<String>ifMinutes=new ArrayList<String>();
			
			String time1Seheri="0",signSeheri="+",time1Iftar="0",signIftar="+";
			for(int i=0;i<distNames.size();i++)
			{
				if(distNames.get(i).equals(region))
				{
					regionPos=i;
					time1Seheri=distSeheriTimes.get(i);
					signSeheri=distSeheriSigns.get(i);
					time1Iftar=distIftarTimes.get(i);
					signIftar=distIftarSigns.get(i);
					break;
				}
			}
			int timeSeheri=Integer.valueOf(time1Seheri);
			int timeIftar=Integer.valueOf(time1Iftar);
			int seMinute=0,seHour=0,ifMinute=0,ifHour=0;
			for(int i=0;i<dhakaIds.size();i++)
			{
				seHour=Integer.valueOf(dhakaSeHours.get(i));
				seMinute=Integer.valueOf(dhakaSeMinutes.get(i));
				ifHour=Integer.valueOf(dhakaIfHours.get(i));
				ifMinute=Integer.valueOf(dhakaIfMinutes.get(i));
				
				if(signSeheri.equals("+"))
				{
					seMinute=seMinute+timeSeheri;
					if(seMinute>59)
					{
						seMinute=seMinute-60;
						seHour=seHour+1;
					}
				}
				else
				{
					seMinute=seMinute-timeSeheri;
					if(seMinute<0)
					{
						seMinute=60+seMinute;
						seHour=seHour-1;
					}
				}
				if(signIftar.equals("+"))
				{
					ifMinute=ifMinute+timeIftar;
					if(ifMinute>59)
					{
						ifMinute=ifMinute-60;
						ifHour=ifHour+1;
					}
				}
				else
				{
					ifMinute=ifMinute-timeIftar;
					if(ifMinute<0)
					{
						ifMinute=60+ifMinute;
						ifHour=ifHour-1;
					}
				}
				seHours.add(String.valueOf(seHour));
				seMinutes.add(String.valueOf(seMinute));
				ifHours.add(String.valueOf(ifHour));
				ifMinutes.add(String.valueOf(ifMinute));
				
			}
			
			Intent in=new Intent(OptionActivity.this,AlarmActivity.class);
			in.putExtra("day",0);
			in.putExtra("seHours", seHours);
			in.putExtra("seMinutes", seMinutes);
			in.putExtra("ifHours", ifHours);
			in.putExtra("ifMinutes", ifMinutes);
			in.putExtra("ids", dhakaIds);
			startActivity(in);
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		}
		else if(view.getId()==R.id.linearMosque)
		{
			scroll.scrollTo((scroll.getRight()/8)*5, 0);
			showMosques();
		}
		else if(view.getId()==R.id.linearDoa)
		{
			scroll.scrollTo((scroll.getRight()/8)*8, 0);
			Intent in=new Intent(OptionActivity.this,DoaActivity.class);
			startActivity(in);
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		}
		else if(view.getId()==R.id.linearRoja)
		{
			scroll.scrollTo((scroll.getRight()/8)*11, 0);
			Intent in=new Intent(OptionActivity.this,RojaActivity.class);
			startActivity(in);
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		}
		else if(view.getId()==R.id.linearTosbi)
		{
			scroll.scrollTo((scroll.getRight()/8)*14, 0);
			Intent in=new Intent(OptionActivity.this,TosbiActivity.class);
			startActivity(in);
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		}
		else if(view.getId()==R.id.linearSMS)
		{
			scroll.scrollTo((scroll.getRight()/8)*18, 0);
			Intent in=new Intent(OptionActivity.this,SMSActivity.class);
			startActivity(in);
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		}
		else if(view.getId()==R.id.linearNirmata)
		{
			scroll.scrollTo((scroll.getRight()/8)*19, 0);
			Intent in=new Intent(OptionActivity.this,NirmataActivity.class);
			startActivity(in);
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		}
	}
	
	
	
	
	
	public void showMosques()
	{
		WifiManager wifi=(WifiManager)getSystemService(Context.WIFI_SERVICE);
		boolean wifiEnabled=wifi.isWifiEnabled();
		if(isConnectingInternet()||wifiEnabled)
		{
			LocationManager locManager=(LocationManager)getSystemService(Context.LOCATION_SERVICE);
			boolean gps=locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
			boolean network=locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			if(gps||network)
			{
				try
				{
					LayoutInflater li = LayoutInflater.from(OptionActivity.this);
					View promptsView = li.inflate(R.layout.loading_dialog, null);
					TextView txtLoad=(TextView)promptsView.findViewById(R.id.txtLoad);
					txtLoad.setTypeface(banglaFont);
					String loadingText="আশেপাশের মসজিদ সার্চ করা হচ্ছে ...";
					if(supported)
					{
						SpannableString convertedLoadingText=AndroidCustomFontSupport.getCorrectedBengaliFormat(loadingText, banglaFont, (float)1);
						txtLoad.setText(convertedLoadingText);
					}
					else txtLoad.setText(loadingText);
					AlertDialog.Builder builder = new AlertDialog.Builder(OptionActivity.this);
					builder.setView(promptsView);
					builder.setCancelable(false);
				    progress = builder.create();
					progress.show();
					
					locationClient=new LocationClient(this, this, this);
					count=0;
					locationClient.connect();
				}
				catch(Exception e)
				{
					showToast("আপনার লোকেশন পাওয়া যায় নি");
				}
			}
			else
			{
				LayoutInflater inflater=LayoutInflater.from(OptionActivity.this);
				View view=inflater.inflate(R.layout.warning_dialog, null);
				LinearLayout linearYes,linearNo;
				TextView txtYes,txtNo,txtDialogTitle,txtDialogBody;
				linearYes=(LinearLayout)view.findViewById(R.id.linearYes);
				linearNo=(LinearLayout)view.findViewById(R.id.linearNo);
				txtYes=(TextView)view.findViewById(R.id.txtYes);
				txtNo=(TextView)view.findViewById(R.id.txtNo);
				txtDialogTitle=(TextView)view.findViewById(R.id.txtDialogTitle);
				txtDialogBody=(TextView)view.findViewById(R.id.txtDialogBody);
				Typeface banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
				
				txtDialogBody.setTypeface(banglaFont);
				txtDialogTitle.setTypeface(banglaFont);
				txtYes.setTypeface(banglaFont);
				txtNo.setTypeface(banglaFont);
				
				String header="সতর্কবার্তা";
				String ok="ঠিক আছে";
				String cancel="বাতিল";
				String warning="আপনার মোবাইল ফোনের যে কোন একটি  লোকেশন সার্ভিস: জিপিএস অথবা মোবাইল নেটওয়ার্ক অন করতে ঠিক আছে বাটনে ক্লিক করুন।";
				
				if(supported)
				{
					SpannableString convertedHeader=AndroidCustomFontSupport.getCorrectedBengaliFormat(header, banglaFont, (float)1);
					txtDialogTitle.setText(convertedHeader);
					SpannableString convertedWarning=AndroidCustomFontSupport.getCorrectedBengaliFormat(warning, banglaFont, (float)1);
					txtDialogBody.setText(convertedWarning);
					SpannableString convertedOk=AndroidCustomFontSupport.getCorrectedBengaliFormat(ok, banglaFont, (float)1);
					txtYes.setText(convertedOk);
					SpannableString convertedCancel=AndroidCustomFontSupport.getCorrectedBengaliFormat(cancel, banglaFont, (float)1);
					txtNo.setText(convertedCancel);
				}
				else
				{
					txtDialogTitle.setText(header);
					txtDialogBody.setText(warning);
					txtYes.setText(ok);
					txtNo.setText(cancel);
				}
				
				AlertDialog.Builder builder=new AlertDialog.Builder(OptionActivity.this);
				builder.setView(view);
				builder.setCancelable(false);
				
			    dialog=builder.create();
				dialog.show();
				
				linearYes.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub	
						dialog.cancel();
						Intent in=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						startActivity(in);
					}
				});
				
				
				linearNo.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});
			}
		}
		else
		{
			LayoutInflater inflater=LayoutInflater.from(OptionActivity.this);
			View view=inflater.inflate(R.layout.warning_dialog, null);
			LinearLayout linearYes,linearNo;
			TextView txtYes,txtNo,txtDialogTitle,txtDialogBody;
			linearYes=(LinearLayout)view.findViewById(R.id.linearYes);
			linearNo=(LinearLayout)view.findViewById(R.id.linearNo);
			txtYes=(TextView)view.findViewById(R.id.txtYes);
			txtNo=(TextView)view.findViewById(R.id.txtNo);
			txtDialogTitle=(TextView)view.findViewById(R.id.txtDialogTitle);
			txtDialogBody=(TextView)view.findViewById(R.id.txtDialogBody);
			
			Typeface banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
			
			txtDialogBody.setTypeface(banglaFont);
			txtDialogTitle.setTypeface(banglaFont);
			txtYes.setTypeface(banglaFont);
			txtNo.setTypeface(banglaFont);
			
			String header="সতর্কবার্তা";
			String ok="ঠিক আছে";
			String cancel="বাতিল";
			String warning="আপনার মোবাইল ফোনের ইন্টারনেট কানেকশন অফ করা আছে। এটি অন করতে ঠিক আছে বাটনে ক্লিক করুন।";
			
			if(supported)
			{
				SpannableString convertedHeader=AndroidCustomFontSupport.getCorrectedBengaliFormat(header, banglaFont, (float)1);
				txtDialogTitle.setText(convertedHeader);
				SpannableString convertedWarning=AndroidCustomFontSupport.getCorrectedBengaliFormat(warning, banglaFont, (float)1);
				txtDialogBody.setText(convertedWarning);
				SpannableString convertedOk=AndroidCustomFontSupport.getCorrectedBengaliFormat(ok, banglaFont, (float)1);
				txtYes.setText(convertedOk);
				SpannableString convertedCancel=AndroidCustomFontSupport.getCorrectedBengaliFormat(cancel, banglaFont, (float)1);
				txtNo.setText(convertedCancel);
			}
			else
			{
				txtDialogTitle.setText(header);
				txtDialogBody.setText(warning);
				txtYes.setText(ok);
				txtNo.setText(cancel);
			}
			
			AlertDialog.Builder builder=new AlertDialog.Builder(OptionActivity.this);
			builder.setView(view);
			builder.setCancelable(false);
			
		    dialog=builder.create();
			dialog.show();
			
			linearYes.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub	
					dialog.cancel();
					Intent in=new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS);
					startActivity(in);
				}
			});
			
			
			linearNo.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					dialog.cancel();
				}
			});
		}
	}
	
	
	
	public void showToast(String text)
	{
		LayoutInflater li = LayoutInflater.from(this);
		View view = li.inflate(R.layout.custom_toast, null);
		TextView txtToast=(TextView)view.findViewById(R.id.txtToast);
		txtToast.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			txtToast.setText(convertedText);
		}
		else
		{
			txtToast.setText(text);
		}
		Toast toast=new Toast(this);
		toast.setView(view);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.show();
	}
	
	
	
	public boolean isConnectingInternet()
	{
		ConnectivityManager connectivity=(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
	    if(connectivity!=null)
	    {
	    	NetworkInfo info=connectivity.getActiveNetworkInfo();
	    	if(info!=null&&info.isConnected()) return true;
	    }
		return false;
	}
	
	
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		
		LayoutInflater inflater=LayoutInflater.from(OptionActivity.this);
		View view=inflater.inflate(R.layout.warning_dialog, null);
		LinearLayout linearYes,linearNo;
		TextView txtYes,txtNo,txtDialogTitle,txtDialogBody;
		linearYes=(LinearLayout)view.findViewById(R.id.linearYes);
		linearNo=(LinearLayout)view.findViewById(R.id.linearNo);
		txtYes=(TextView)view.findViewById(R.id.txtYes);
		txtNo=(TextView)view.findViewById(R.id.txtNo);
		txtDialogTitle=(TextView)view.findViewById(R.id.txtDialogTitle);
		txtDialogBody=(TextView)view.findViewById(R.id.txtDialogBody);
		Typeface banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		txtDialogTitle.setTypeface(banglaFont);
		txtDialogBody.setTypeface(banglaFont);
		txtYes.setTypeface(banglaFont);
		txtNo.setTypeface(banglaFont);
		
		String dialogTitle="সতর্কবার্তা";
		String dialogBody="আপনি কি এই মুহূর্তে প্রস্থান করতে চান?";
		String yes="প্রস্থান";
		String no="বাতিল";
		
		if(supported)
		{
			SpannableString convertedDialogTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(dialogTitle, banglaFont, (float)1);
			txtDialogTitle.setText(convertedDialogTitle);
			SpannableString convertedDialogBody=AndroidCustomFontSupport.getCorrectedBengaliFormat(dialogBody, banglaFont, (float)1);
			txtDialogBody.setText(convertedDialogBody);
			SpannableString convertedYes=AndroidCustomFontSupport.getCorrectedBengaliFormat(yes, banglaFont, (float)1);
			txtYes.setText(convertedYes);
			SpannableString convertedNo=AndroidCustomFontSupport.getCorrectedBengaliFormat(no, banglaFont, (float)1);
			txtNo.setText(convertedNo);
		}
		else
		{
			txtDialogTitle.setText(dialogTitle);
			txtDialogBody.setText(dialogBody);
			txtYes.setText(yes);
			txtNo.setText(no);
		}
		
		AlertDialog.Builder builder=new AlertDialog.Builder(OptionActivity.this);
		builder.setView(view);
		builder.setCancelable(true);
		
		final AlertDialog dialog=builder.create();
		dialog.show();
		
        linearYes.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
				System.exit(1);
			}
		});
		
		
		linearNo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});
		
	}
	
	
	
	
	
	public String readConnectionString(String URL) {
		StringBuilder stringBuilder = new StringBuilder();
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(URL);
		try {
			HttpResponse response = httpClient.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream inputStream = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inputStream));
				String line;
				while ((line = reader.readLine()) != null) {
					stringBuilder.append(line);
				}
				inputStream.close();
			} else {
				Log.d("ConnectionString", "Failed to connect");
			}
		} catch (Exception e) {
			Log.d("ConnectionString", e.getLocalizedMessage());
		}
		return stringBuilder.toString();
	}
	
	
	
	
	class PlaceFinder extends AsyncTask<String, Void, String>
	{
		
		ArrayList<String>names=new ArrayList<String>();
		ArrayList<String>ids=new ArrayList<String>();
		ArrayList<String>lats=new ArrayList<String>();
		ArrayList<String>lons=new ArrayList<String>();

		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
		}
		
		
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			return readConnectionString(params[0]);
		}
		
		
		@Override
		protected void onPostExecute(String JSONString) {
			// TODO Auto-generated method stub
			try {
				JSONObject jsonObject = new JSONObject(JSONString);
				JSONArray placeItems = new JSONArray(jsonObject.getString("results"));
				for(int i=0;i<placeItems.length();i++)
				{
					JSONObject placeItem = placeItems.getJSONObject(i);
					Double lat = Double.parseDouble(placeItem.getJSONObject("geometry").getJSONObject("location").getString("lat"));
					Double lon = Double.parseDouble(placeItem.getJSONObject("geometry").getJSONObject("location").getString("lng"));
					String name = placeItem.getString("name");
					String id=placeItem.getString("place_id");
					names.add(name);
					ids.add(id);
					lats.add(String.valueOf(lat));
					lons.add(String.valueOf(lon));
				}
				progress.dismiss();
				
				Intent in=new Intent(OptionActivity.this,MosqueActivity.class);
				in.putExtra("names", names);
				in.putExtra("ids", ids);
				in.putExtra("lats", lats);
				in.putExtra("lons", lons);
				in.putExtra("lat", mLat);
				in.putExtra("lon", mLon);
				startActivity(in);
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
	}





	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		count++;
		try
		{
			Location location=locationClient.getLastLocation();
				mLat=location.getLatitude();
				mLon=location.getLongitude();
				
				String placeURL="https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="
				        +mLat
						+ ","
						+ mLon
						+ "&radius=1000&types=mosque&sensor=false&key="+API_KEY;
				PlaceFinder finder=new PlaceFinder();
				finder.execute(placeURL); 
				
		}
		catch(Exception e)
		{
			if(count<5)
			{
				locationClient.disconnect();
				locationClient.connect();
			}
			else
			{
				progress.dismiss();
				showToast("আপনার লোকেশন পাওয়া যায় নি");
				//Toast.makeText(getApplicationContext(), "Can't find your location", Toast.LENGTH_LONG).show();
			}
		}
	}



	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
