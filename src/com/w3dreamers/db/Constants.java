package com.w3dreamers.db;

import java.util.ArrayList;

public class Constants {
//-------------------------------------------------------------	
    public static final int DATABASE_VERSION = 1;
    
    public static final int STRING_ONLY=1;
    public static final int STRING_ANY=2;
    public static final int STRING_FIRST=3;
    public static final int STRING_LAST=4;
    
    public static final String IS_EQUAL="=";
    public static final String IS_NOT_EQUAL="!=";
    public static final String IS_GREATER=">";
    public static final String IS_LESS="<";
    public static final String IS_GREATER_EQUAL=">=";
    public static final String IS_LESS_EQUAL="<=";
    
    public static final int IS_NUMBER=1;
    public static final int IS_STRING=2;
    
    //-----------------------------------------------------------
    
    
    // Database Name
    public static final String DATABASE_NAME = "ramadan.db";
 
    // Contacts table name
    public static final String TABLE_DISTRICT_TIME = "district_time"; 
    public static final String DISTRICT_TIME_ID="id";
    public static final String DISTRICT_TIME_NAME="name";
    public static final String DISTRICT_TIME_TIME_SEHERI="time_seheri";
    public static final String DISTRICT_TIME_SIGN_SEHERI="sign_seheri";
    public static final String DISTRICT_TIME_TIME_IFTAR="time_iftar";
    public static final String DISTRICT_TIME_SIGN_IFTAR="sign_iftar";
    
    
    public static final String TABLE_DHAKA_TIME = "dhaka_time"; 
    public static final String DHAKA_TIME_ID="id";
    public static final String DHAKA_TIME_DATE="date";
    public static final String DHAKA_TIME_SEHERY_HOUR="sehery_hour";
    public static final String DHAKA_TIME_SEHERY_MINUTE="sehery_minute";
    public static final String DHAKA_TIME_IFTAR_HOUR="iftar_hour";
    public static final String DHAKA_TIME_IFTAR_MINUTE="iftar_minute";
    
    
    
    public ArrayList<String> getColumnName(String tableName)
    {
    	ArrayList<String> allColumn=new ArrayList<String>();
    	allColumn.clear();
    	if(tableName.equals(this.TABLE_DISTRICT_TIME))
    	{
    		allColumn.add(DISTRICT_TIME_ID);
    		allColumn.add(DISTRICT_TIME_NAME);
    		allColumn.add(DISTRICT_TIME_TIME_SEHERI);
    		allColumn.add(DISTRICT_TIME_SIGN_SEHERI);
    		allColumn.add(DISTRICT_TIME_TIME_IFTAR);
    		allColumn.add(DISTRICT_TIME_SIGN_IFTAR);
    	}
    	else if(tableName.equals(this.TABLE_DHAKA_TIME))
    	{
    		allColumn.add(DHAKA_TIME_ID);
    		allColumn.add(DHAKA_TIME_DATE);
    		allColumn.add(DHAKA_TIME_SEHERY_HOUR);
    		allColumn.add(DHAKA_TIME_SEHERY_MINUTE);
    		allColumn.add(DHAKA_TIME_IFTAR_HOUR);
    		allColumn.add(DHAKA_TIME_IFTAR_MINUTE);
    	}
   	
    	return allColumn;
    }

}
