package com.w3dreamers.romadan2015;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MosqueActivity extends FragmentActivity{
	
	LinearLayout linearMap;
	TextView txtTitle;
	ListView list;
	
	boolean supported;
	Typeface banglaFont;
	
	double myLat,myLon;
	
	ArrayList<String>names=new ArrayList<String>();
	ArrayList<String>ids=new ArrayList<String>();
	ArrayList<String>lats=new ArrayList<String>();
	ArrayList<String>lons=new ArrayList<String>();
	
	String name,lat,lon;
	
	MyAdapter adapter;
	
	String API_KEY="AIzaSyAPrOxAoTKUdaXtktg4B2QrdPZO5SpM0VQ";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.mosque_layout);		
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		list=(ListView)findViewById(R.id.listView1);
		linearMap=(LinearLayout)findViewById(R.id.linearMap);
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		
		myLat=getIntent().getExtras().getDouble("lat");
		myLon=getIntent().getExtras().getDouble("lon");	
		
		ids=(ArrayList<String>)getIntent().getSerializableExtra("ids");
		names=(ArrayList<String>)getIntent().getSerializableExtra("names");
		lats=(ArrayList<String>)getIntent().getSerializableExtra("lats");
		lons=(ArrayList<String>)getIntent().getSerializableExtra("lons");	
		
		txtTitle.setTypeface(banglaFont);
        String title="মসজিদ";
		
		if(supported)
		{
			SpannableString convertedTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(title, banglaFont, (float)1);
			txtTitle.setText(convertedTitle);
		}
		else txtTitle.setText(title);
		
		adapter=new MyAdapter(this, names);
		
		list.setAdapter(adapter);
		
		
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				if(isMapAvailable())
				{
					String id=ids.get(position);
					name=names.get(position);
					lat=lats.get(position);
					lon=lons.get(position);
					
					String placeURL="https://maps.googleapis.com/maps/api/place/details/json?placeid=";
					placeURL+=id+"&key="+API_KEY;
					PlaceDetailsFinder finder=new PlaceDetailsFinder();
					finder.execute(placeURL);
				}
			}
		});
		
		
		
		linearMap.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(isMapAvailable())
				{
					Intent in=new Intent(MosqueActivity.this,MapActivity.class);
					in.putExtra("names", names);
					in.putExtra("ids", ids);
					in.putExtra("lats", lats);
					in.putExtra("lons", lons);
					in.putExtra("lat", myLat);
					in.putExtra("lon", myLon);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
			}
		});
		
		
		
	}
	
	
	
	public boolean isMapAvailable()
	{
		int result=GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if(result==ConnectionResult.SUCCESS)
		{
			return true;
		}
		else if(GooglePlayServicesUtil.isUserRecoverableError(result))
		{
			Dialog d=GooglePlayServicesUtil.getErrorDialog(result, this,1);
			d.show();
			showToast("আপানার ডিভাইসে গুগল প্লে সারভিস আপডেট করা নাই");
		}
		else
		{
			showToast("আপানার ডিভাইসে গুগল প্লে সারভিস সাপোর্ট করে না");
		}
		return false;
	}
	
	
	
	public void showToast(String text)
	{
		LayoutInflater li = LayoutInflater.from(this);
		View view = li.inflate(R.layout.custom_toast, null);
		TextView txtToast=(TextView)view.findViewById(R.id.txtToast);
		txtToast.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			txtToast.setText(convertedText);
		}
		else
		{
			txtToast.setText(text);
		}
		Toast toast=new Toast(this);
		toast.setView(view);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.show();
	}
	
	
	
	
	
	public String readConnectionString(String URL) {
		StringBuilder stringBuilder = new StringBuilder();
		HttpClient httpClient = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(URL);
		try {
			HttpResponse response = httpClient.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream inputStream = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(inputStream));
				String line;
				while ((line = reader.readLine()) != null) {
					stringBuilder.append(line);
				}
				inputStream.close();
			} else {
				Log.d("ConnectionString", "Failed to connect");
			}
		} catch (Exception e) {
			Log.d("ConnectionString", e.getLocalizedMessage());
		}
		return stringBuilder.toString();
	}
	
	
	
	
	class PlaceDetailsFinder extends AsyncTask<String, Void, String>
	{
	   AlertDialog progress;

		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			LayoutInflater li = LayoutInflater.from(MosqueActivity.this);
			View promptsView = li.inflate(R.layout.loading_dialog, null);
			TextView txtLoad=(TextView)promptsView.findViewById(R.id.txtLoad);
			txtLoad.setTypeface(banglaFont);
			String loadingText="আশেপাশের মসজিদ সার্চ করা হচ্ছে ...";
			if(supported)
			{
				SpannableString convertedLoadingText=AndroidCustomFontSupport.getCorrectedBengaliFormat(loadingText, banglaFont, (float)1);
				txtLoad.setText(convertedLoadingText);
			}
			else txtLoad.setText(loadingText);
			AlertDialog.Builder builder = new AlertDialog.Builder(MosqueActivity.this);
			builder.setView(promptsView);
			builder.setCancelable(false);
		    progress = builder.create();
			progress.show();
		}
		
		
		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			return readConnectionString(params[0]);
		}
		
		
		@Override
		protected void onPostExecute(String JSONString) {
			// TODO Auto-generated method stub
			try {
				JSONObject jsonObject = new JSONObject(JSONString);
				
				String address=jsonObject.getJSONObject("result").getString("formatted_address");
				
				progress.dismiss();
				Intent intent=new Intent(MosqueActivity.this,EachMosqueActivity.class);
				intent.putExtra("name", name);
				intent.putExtra("lat", lat);
				intent.putExtra("lon", lon);
				intent.putExtra("address", address);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
	}
	
	
	
	

}
