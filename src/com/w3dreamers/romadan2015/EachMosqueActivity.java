package com.w3dreamers.romadan2015;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

public class EachMosqueActivity extends FragmentActivity{
	
	
	TextView txtTitle,txtHeader,txtAddress;
	String name,latitude,longitude,address;
	double lat,lon;
	
	GoogleMap map;
	LatLng loc;
	Marker marker;
	
	boolean supported;
	Typeface banglaFont;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.each_mosque);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		txtHeader=(TextView)findViewById(R.id.txtHeader);
		txtAddress=(TextView)findViewById(R.id.txtAddress);
		
		name=getIntent().getExtras().getString("name");
		latitude=getIntent().getExtras().getString("lat");
		longitude=getIntent().getExtras().getString("lon");
		address=getIntent().getExtras().getString("address");
		
		lat=Double.valueOf(latitude);
		lon=Double.valueOf(longitude);
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		txtTitle.setTypeface(banglaFont);
		txtAddress.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(name, banglaFont, (float)1);
			txtTitle.setText(convertedTitle);
			SpannableString convertedAddress=AndroidCustomFontSupport.getCorrectedBengaliFormat(address, banglaFont, (float)1);
			txtAddress.setText(convertedAddress);
		}
		else
		{
			txtTitle.setText(name);
			txtAddress.setText(address);
		}
		
		
		try
		{
			 map=((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
				
				if(map!=null)
				{
					loc=new LatLng(lat, lon);
					marker=map.addMarker(new MarkerOptions().position(loc).title(name));
					marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN));
					marker.showInfoWindow();
					map.moveCamera(CameraUpdateFactory.newLatLngZoom(loc, 12.0f));
				}
		}
		catch(Exception e)
		{
			showToast("দুঃখিত, ম্যাপ দেখানো সম্ভব হচ্ছে না");
		}
		
		
		
		map.setInfoWindowAdapter(new InfoWindowAdapter() {
			
			@Override
			public View getInfoWindow(Marker marker) {
				// TODO Auto-generated method stub
				 View v=getLayoutInflater().inflate(R.layout.infowindow, null);
				 TextView txtWindowHeader=(TextView)v.findViewById(R.id.txtHeader);
				 txtWindowHeader.setTypeface(banglaFont);
				 String markerTitle=marker.getTitle();
				 if(supported)
				 {
					 SpannableString convertedMarkerTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(markerTitle, banglaFont, (float)1);
				     txtWindowHeader.setText(convertedMarkerTitle);
				 }
				 else
				 {
					 txtWindowHeader.setText(markerTitle);
				 }
				 return v;
			}
			
			@Override
			public View getInfoContents(Marker marker) {
				// TODO Auto-generated method stub
				 return null;
			}
		});
		
		
		
	}
	
	
	
	public void showToast(String text)
	{
		LayoutInflater li = LayoutInflater.from(this);
		View view = li.inflate(R.layout.custom_toast, null);
		TextView txtToast=(TextView)view.findViewById(R.id.txtToast);
		txtToast.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			txtToast.setText(convertedText);
		}
		else
		{
			txtToast.setText(text);
		}
		Toast toast=new Toast(this);
		toast.setView(view);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.show();
	}
	

}
