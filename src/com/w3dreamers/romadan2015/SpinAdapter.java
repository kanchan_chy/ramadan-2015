package com.w3dreamers.romadan2015;

import java.util.ArrayList;
import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class SpinAdapter extends ArrayAdapter<String>{
	
	Context context;
	ArrayList<String>dists=new ArrayList<String>();
	int which;
	
	boolean supported;
	Typeface banglaFont;
	
	LayoutInflater inflater;

	public SpinAdapter(Context context, ArrayList<String>dists,int which) {
		super(context, R.layout.spinner_item, dists);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.dists=dists;
		this.which=which;
		
		SharedPreferences prefSupport=context.getSharedPreferences("BanglaLibrary", 0);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(context.getAssets(), "font/solaimanlipinormal.ttf");
		
		inflater = LayoutInflater.from(context);
	}
	
	
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder=null;
		if(convertView==null)
		{
			convertView=inflater.inflate(R.layout.spinner_item, null);
		  
		    holder= new ViewHolder();
		    holder.txtItem=(TextView)convertView.findViewById(R.id.txtItem);
		    convertView.setTag(holder);
		}
		else
		{   
			    holder=(ViewHolder)convertView.getTag();
		}
		
		holder.txtItem.setTypeface(banglaFont);
		String text=dists.get(position);
		if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			holder.txtItem.setText(convertedText);
		}
		else holder.txtItem.setText(text);
		
		return convertView;
	}  
	
	@Override
	public View getView(int position, View cnvtView, ViewGroup parent) {
		View mySpinner = inflater.inflate(R.layout.drop_down_item, parent,false);
		TextView txtItem = (TextView) mySpinner.findViewById(R.id.txtItem);
		String text;
		if(which==1)
		{
			if(OptionActivity.clicked) text=dists.get(position);
			else text=dists.get(OptionActivity.regionPos);
		}
		else
		{
			text=dists.get(position);
		}
		txtItem.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			txtItem.setText(convertedText);
		}
		else txtItem.setText(text);

		return mySpinner;
	}
	
	/*
	public View getCustomView(int position, View convertView,
			ViewGroup parent) {
		LayoutInflater inflater = LayoutInflater.from(context);
		View mySpinner = inflater.inflate(R.layout.spinner_item, parent,false);
		TextView txtItem = (TextView) mySpinner.findViewById(R.id.txtItem);
		txtItem.setText(dists[position]);

		return mySpinner;
	}
	*/
	
	
	
	public static class ViewHolder {
	    public TextView txtItem;
	    
	}
	

}
