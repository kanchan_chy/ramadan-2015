package com.w3dreamers.romadan2015;

import java.util.ArrayList;
import java.util.Calendar;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;

import android.R.integer;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class AlarmActivity extends Activity{
	
	TextView txtTitle,txtHeader,txtSeheri,txtIftar,txtVibrate;
	Spinner spinnerSeheri,spinnerIftar;
	ToggleButton toggleSeheri,toggleIftar;
	ImageView imgVibrate;
	LinearLayout linearRingtone;
//	CheckBox checkVibrate;
	
	final String[] banglaMonths = {"","জানুয়ারি", "ফেব্রুয়ারি", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টোবর", "নভেম্বর", "ডিসেম্বর"};
	final String[] banglaNums={"০","১","২","৩","৪","৫","৬","৭","৮","৯","১০","১১","১২","১৩","১৪","১৫","১৬","১৭","১৮","১৯","২০","২১","২২","২৩","২৪","২৫","২৬","২৭","২৮","২৯","৩০","৩১","৩২","৩৩","৩৪","৩৫","৩৬","৩৭","৩৮","৩৯","৪০","৪১","৪২","৪৩","৪৪","৪৫","৪৬","৪৭","৪৮","৪৯","৫০","৫১","৫২","৫৩","৫৪","৫৫","৫৬","৫৭","৫৮","৫৯","৬০"};
	
	ArrayList<String>seHours=new ArrayList<String>();
	ArrayList<String>seMinutes=new ArrayList<String>();
	ArrayList<String>ifHours=new ArrayList<String>();
	ArrayList<String>ifMinutes=new ArrayList<String>();
	ArrayList<String>ids=new ArrayList<String>();
	
	int day,month=0,year,seHour=0,seMinute=0,ifHour=0,ifMinute=0,id=0,rojaSerial=0,rojaDay,rojaMonth;
	
	boolean supported;
	Typeface banglaFont;
	
	SpinAdapter seheriAdapter,iftarAdapter;
	ArrayList<String> seheriOptions=new ArrayList<String>();
	ArrayList<String> iftarOptions=new ArrayList<String>();
	
	ArrayList<Integer>seheriTimes=new ArrayList<Integer>();
	ArrayList<Integer>iftarTimes=new ArrayList<Integer>();
	
	private static final String ACTION = "com.w3dreamers.ramadan.ALARM";
	boolean seheriStatus,iftarStatus;
	int seheriPos,iftarPos;
	
	AlarmManager manager;
	Intent alarmIntent;
	
	String on,off;
	
	SharedPreferences prefAlarm;
	SharedPreferences.Editor alarmEditor;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.alarm_layout);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		txtHeader=(TextView)findViewById(R.id.txtHeader);
		txtSeheri=(TextView)findViewById(R.id.txtSeheri);
		txtIftar=(TextView)findViewById(R.id.txtIftar);
		txtVibrate=(TextView)findViewById(R.id.txtVibarte);
		spinnerSeheri=(Spinner)findViewById(R.id.spinnerSeheri);
		spinnerIftar=(Spinner)findViewById(R.id.spinnerIftar);
		toggleSeheri=(ToggleButton)findViewById(R.id.toggleSeheri);
		toggleIftar=(ToggleButton)findViewById(R.id.toggleIftar);
		imgVibrate=(ImageView)findViewById(R.id.imgVibrate);
		linearRingtone=(LinearLayout)findViewById(R.id.linearRingtone);
		
		String title="অ্যালার্ম";
		String header="";
		String seheri="সেহরীর পূর্বে";
		String iftar="ইফতারের পূর্বে";
		String vibrate="ভাইব্রেশন";
		
		manager=(AlarmManager)getSystemService(Context.ALARM_SERVICE);
		alarmIntent=new Intent(ACTION);
		
		year=2015;
		day=getIntent().getExtras().getInt("day");
		if(day!=0)
		{
			month=getIntent().getExtras().getInt("month");
			seHour=getIntent().getExtras().getInt("seHour");
			seMinute=getIntent().getExtras().getInt("seMinute");
			ifHour=getIntent().getExtras().getInt("ifHour");
			ifMinute=getIntent().getExtras().getInt("ifMinute");
			id=getIntent().getExtras().getInt("id");
			
			header=banglaMonths[month]+" "+banglaNums[day]+", ";
			String sYear=String.valueOf(year);
			for(int i=0;i<sYear.length();i++)
			{
				String a=""+sYear.charAt(i);
				int b=Integer.valueOf(a);
				header+=banglaNums[b];
			}
			header+=" ইংরেজি তারিখের রোজার জন্য অ্যালার্ম সেট করুন";
			rojaDay=day;
			rojaMonth=month;
			if(rojaMonth==6) rojaSerial=rojaDay-18;
			if(rojaMonth==7) rojaSerial=12+rojaDay;
		}
		else
		{
			seHours=(ArrayList<String>)getIntent().getSerializableExtra("seHours");
			seMinutes=(ArrayList<String>)getIntent().getSerializableExtra("seMinutes");
			ifHours=(ArrayList<String>)getIntent().getSerializableExtra("ifHours");
			ifMinutes=(ArrayList<String>)getIntent().getSerializableExtra("ifMinutes");
			ids=(ArrayList<String>)getIntent().getSerializableExtra("ids");
			
			header="প্রতিটি রোজার জন্য অ্যালার্ম সেট করুন";
			
			Calendar cal=Calendar.getInstance();
			rojaDay=cal.get(Calendar.DAY_OF_MONTH);
			rojaMonth=cal.get(Calendar.MONTH)+1;
			if(rojaMonth==6) rojaSerial=rojaDay-18;
			if(rojaMonth==7) rojaSerial=12+rojaDay;
			
		}
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		
		txtTitle.setTypeface(banglaFont);
		txtHeader.setTypeface(banglaFont);
		txtSeheri.setTypeface(banglaFont);
		txtIftar.setTypeface(banglaFont);
		txtVibrate.setTypeface(banglaFont);
		
		if(supported)
		{
			SpannableString convertedTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(title, banglaFont, (float)1);
			txtTitle.setText(convertedTitle);
			SpannableString convertedHeader=AndroidCustomFontSupport.getCorrectedBengaliFormat(header, banglaFont, (float)1);
			txtHeader.setText(convertedHeader);
			SpannableString convertedSeheri=AndroidCustomFontSupport.getCorrectedBengaliFormat(seheri, banglaFont, (float)1);
			txtSeheri.setText(convertedSeheri);
			SpannableString convertedIftar=AndroidCustomFontSupport.getCorrectedBengaliFormat(iftar, banglaFont, (float)1);
			txtIftar.setText(convertedIftar);
			SpannableString convertedVibrate=AndroidCustomFontSupport.getCorrectedBengaliFormat(vibrate, banglaFont, (float)1);
			txtVibrate.setText(convertedVibrate);
		}
		else
		{
			txtTitle.setText(title);
			txtHeader.setText(header);
			txtSeheri.setText(seheri);
			txtIftar.setText(iftar);
		}
		
		prefAlarm=getSharedPreferences("Alarm", MODE_PRIVATE);
		alarmEditor=prefAlarm.edit();
		
		on="অন";
		off="অফ";
		
		toggleSeheri.setTypeface(banglaFont);
		toggleIftar.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedOn=AndroidCustomFontSupport.getCorrectedBengaliFormat(on, banglaFont, (float)1);
			SpannableString convertedOff=AndroidCustomFontSupport.getCorrectedBengaliFormat(off, banglaFont, (float)1);
			toggleSeheri.setTextOn(convertedOn);
			toggleIftar.setTextOn(convertedOn);
			toggleSeheri.setTextOff(convertedOff);
			toggleIftar.setTextOff(convertedOff);
		}
		else
		{
			toggleSeheri.setTextOn(on);
			toggleIftar.setTextOn(on);
			toggleSeheri.setTextOff(off);
			toggleIftar.setTextOff(off);
		}
		
		
		SharedPreferences vibPref=getSharedPreferences("Vivration", MODE_PRIVATE);
		boolean status=vibPref.getBoolean("vibrate", false);
		if(status) imgVibrate.setImageResource(R.drawable.checkbox_checked);
		else imgVibrate.setImageResource(R.drawable.checkbox_unchecked);		
		
		iftarOptions.add("০ মিনিট");
		iftarOptions.add("৫ মিনিট");
		iftarOptions.add("১০ মিনিট");
		iftarOptions.add("১৫ মিনিট");
		iftarOptions.add("৩০ মিনিট");
		
		seheriOptions.add("৩০ মিনিট");
		seheriOptions.add("১ ঘন্টা");
		seheriOptions.add("১ ঘ. ৩০মি.");
		seheriOptions.add("২ ঘন্টা");
		
		seheriAdapter=new SpinAdapter(this, seheriOptions, 2);
		iftarAdapter=new SpinAdapter(this, iftarOptions, 2);
		
		spinnerSeheri.setAdapter(seheriAdapter);
		spinnerIftar.setAdapter(iftarAdapter);
		
		seheriTimes.add(30);
		seheriTimes.add(60);
		seheriTimes.add(90);
		seheriTimes.add(120);
		
		iftarTimes.add(0);
		iftarTimes.add(5);
		iftarTimes.add(10);
		iftarTimes.add(15);
		iftarTimes.add(30);
		
		seheriStatus=false;
		iftarStatus=false;
		seheriPos=0;
		iftarPos=0;
		if(day!=0)
		{
			String sSeheri=prefAlarm.getString("seheri_one", "-1");
			String sIftar=prefAlarm.getString("iftar_one", "-1");
			if(!sSeheri.equals("-1"))
			{
				int state=0;
				int seheriDay=0;
				int seheriMonth=0;
				int seheriTime=0;
				for(int i=0;i<sSeheri.length();i++)
				{
					if(sSeheri.charAt(i)==':')
					{
						state++;
					}
					else
					{
						if(state==0)
						{
							seheriMonth=seheriMonth*10+(sSeheri.charAt(i)-48);
						}
						else if(state==1)
						{
							seheriDay=seheriDay*10+(sSeheri.charAt(i)-48);
						}
						else
						{
							seheriTime=seheriTime*10+(sSeheri.charAt(i)-48);
						}
					}
				}
				if(day==seheriDay&&month==seheriMonth)
				{					
					for(int i=0;i<seheriTimes.size();i++)
					{
						if(seheriTimes.get(i)==seheriTime)
						{
							seheriPos=i;
							break;
						}
					}
					seheriStatus=isPrevious(seheriPos, seMinute, seHour, true);
			/*		int alarmMinute=seMinute-(seheriTimes.get(seheriPos)%60);
					int alarmHour=seHour-(seheriTimes.get(seheriPos)/60);
					Calendar cal=Calendar.getInstance();
					int currentHour=cal.get(Calendar.HOUR_OF_DAY);
					int currentMinute=cal.get(Calendar.MINUTE);
					int currentDay=cal.get(Calendar.DAY_OF_MONTH);
					int currentMonth=cal.get(Calendar.MONTH);
					
					if(currentMonth<=month)
					{
						if(currentDay<day) seheriStatus=true;
						else if(currentDay==day)
						{
							if(currentHour<alarmHour) seheriStatus=true;
							else if(currentHour==alarmHour)
							{
								if(currentMinute<alarmMinute) seheriStatus=true;
							}
						}
					}    */
					if(seheriStatus==true)
					{
						spinnerSeheri.setSelection(seheriPos);
						toggleSeheri.setChecked(true);
					}
					else toggleSeheri.setChecked(false);
					
				}
				
			}
			if(!sIftar.equals("-1"))
			{
				
				int state=0;
				int iftarDay=0;
				int iftarMonth=0;
				int iftarTime=0;
				for(int i=0;i<sIftar.length();i++)
				{
					if(sIftar.charAt(i)==':')
					{
						state++;
					}
					else
					{
						if(state==0)
						{
							iftarMonth=iftarMonth*10+(sIftar.charAt(i)-48);
						}
						else if(state==1)
						{
							iftarDay=iftarDay*10+(sIftar.charAt(i)-48);
						}
						else
						{
							iftarTime=iftarTime*10+(sIftar.charAt(i)-48);
						}
					}
				}
				if(day==iftarDay&&month==iftarMonth)
				{
					for(int i=0;i<iftarTimes.size();i++)
					{
						if(iftarTimes.get(i)==iftarTime)
						{
							iftarPos=i;
							break;
						}
					}
					iftarStatus=isPrevious(iftarPos, ifMinute, ifHour, false);
				/*	int alarmMinute=ifMinute-(iftarTimes.get(iftarPos)%60);
					int alarmHour=ifHour-(iftarTimes.get(iftarPos)/60);
					alarmHour=alarmHour+12;
					Calendar cal=Calendar.getInstance();
					int currentHour=cal.get(Calendar.HOUR_OF_DAY);
					int currentMinute=cal.get(Calendar.MINUTE);
					int currentDay=cal.get(Calendar.DAY_OF_MONTH);
					int currentMonth=cal.get(Calendar.MONTH);
					
					if(currentMonth<=month)
					{
						if(currentDay<day) iftarStatus=true;
						else if(currentDay==day)
						{
							if(currentHour<alarmHour) iftarStatus=true;
							else if(currentHour==alarmHour)
							{
								if(currentMinute<alarmMinute) iftarStatus=true;
							}
						}
					}  */
					if(iftarStatus==true)
					{
						spinnerIftar.setSelection(iftarPos);
						toggleIftar.setChecked(true);
					}
					else toggleIftar.setChecked(false);
					
				}
				
			}
		}
		else
		{
			int seheriTime=prefAlarm.getInt("seheri_each", -1);
			int iftarTime=prefAlarm.getInt("iftar_each", -1);
			if(seheriTime!=-1)
			{
				for(int i=0;i<seheriTimes.size();i++)
				{
					if(seheriTimes.get(i)==seheriTime)
					{
						seheriPos=i;
						break;
					}
				}
				spinnerSeheri.setSelection(seheriPos);
				toggleSeheri.setChecked(true);
				seheriStatus=true;
			}
			else toggleSeheri.setChecked(false);
			if(iftarTime!=-1)
			{
				for(int i=0;i<iftarTimes.size();i++)
				{
					if(iftarTimes.get(i)==iftarTime)
					{
						iftarPos=i;
						break;
					}
				}
				spinnerIftar.setSelection(iftarPos);
				toggleIftar.setChecked(true);
				iftarStatus=true;
			}
			else toggleIftar.setChecked(false);
			
			for(int i=0;i<ids.size();i++)
			{
				if(Integer.valueOf(ids.get(i))==rojaSerial)
				{
					seMinute=Integer.valueOf(seMinutes.get(i));
					seHour=Integer.valueOf(seHours.get(i));
					ifMinute=Integer.valueOf(ifMinutes.get(i));
					ifHour=Integer.valueOf(ifHours.get(i));
					break;
				}
			}
			
		}
		
		
		imgVibrate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				vibrationClicked();
			}
		});
		
		txtVibrate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				vibrationClicked();
			}
		});
		
		
		linearRingtone.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
				intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_ALARM);
				intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, "Select Tone");
				intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, (Uri) null);
				startActivityForResult(intent, 5);
			}
		});
		
		
	
		spinnerSeheri.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				if(position!=seheriPos&&toggleSeheri.isChecked()==true)
				{
					if(seheriStatus)
					{
						removeSeheriAlarm();
					}
					seheriPos=position;
					setSehriAlarm();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		
		spinnerIftar.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				if(position!=iftarPos&&toggleIftar.isChecked()==true)
				{
					if(iftarStatus)
					{
						removeIftarAlarm();
					}
					iftarPos=position;
					setIftarAlarm();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		
		

		toggleSeheri.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked==false)
				{
					if(seheriStatus)
					{
						removeSeheriAlarm();
					}
				}
				else
				{
					if(seheriStatus)
					{
						removeSeheriAlarm();
					}
					setSehriAlarm();
				}
			}
		});
		
		
		toggleIftar.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				if(isChecked==false)
				{
					if(iftarStatus)
					{
						removeIftarAlarm();
					}
				}
				else
				{
					if(iftarStatus)
					{
						removeIftarAlarm();
					}
					setIftarAlarm();
				}
			}
		});
		
		
		
	}
	
	
	
	public Calendar getCalender(int aMonth,int aDay,int aHour,int aMinute, boolean isSeheri)
	{
		if(isSeheri) aMinute=aMinute-(seheriTimes.get(seheriPos)%60);
		else aMinute=aMinute-(iftarTimes.get(iftarPos)%60);
		if(isSeheri) aHour=aHour-(seheriTimes.get(seheriPos)/60);
		else aHour=aHour-(iftarTimes.get(iftarPos)/60);
		if(isSeheri==false) aHour=aHour+12;
		
		Calendar calAlarm=Calendar.getInstance();
		calAlarm.set(Calendar.YEAR, 2015);
		calAlarm.set(Calendar.MONTH, aMonth-1);
		calAlarm.set(Calendar.DAY_OF_MONTH, aDay);
		calAlarm.set(Calendar.HOUR_OF_DAY, aHour);
		calAlarm.set(Calendar.MINUTE, aMinute);
		calAlarm.set(Calendar.SECOND, 0);
		return calAlarm;
	}
	
	
	
	public void setSehriAlarm()
	{
		seheriPos=spinnerSeheri.getSelectedItemPosition();
		if(day!=0)
		{
			boolean previous=isPrevious(seheriPos, seMinute, seHour, true);
			if(previous)
			{
				PendingIntent pending=PendingIntent.getBroadcast(getApplicationContext(), id, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
				Calendar cal=getCalender(month, day, seHour, seMinute,true);
				manager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pending);
			}
			String ss=month+":"+day+":"+seheriTimes.get(seheriPos);
			alarmEditor.putString("seheri_one", ss);
			alarmEditor.commit();
		}
		else
		{
			boolean previous=isPrevious(seheriPos, seMinute, seHour, true);
			if(previous)
			{
				PendingIntent pending=PendingIntent.getBroadcast(getApplicationContext(), rojaSerial, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
				Calendar cal=getCalender(rojaMonth, rojaDay, seHour, seMinute,true);
				manager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pending);
			}
			for(int i=0;i<ids.size();i++)
			{
				int mId=Integer.valueOf(ids.get(i));
				if(mId>rojaSerial)
				{
					PendingIntent pending=PendingIntent.getBroadcast(getApplicationContext(), mId, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
					int aMonth;
					if(mId<=12) aMonth=6;
					else aMonth=7;
					int aDay=18+mId;
					if(aDay>30) aDay=aDay-30;
					int aHour=Integer.valueOf(seHours.get(i));
					int aMinute=Integer.valueOf(seMinutes.get(i));
					Calendar cal=getCalender(aMonth, aDay, aHour, aMinute,true);
					manager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pending);
				}
			}
			alarmEditor.putInt("seheri_each",seheriTimes.get(seheriPos));
			alarmEditor.commit();
		}
	}
	
	
	public void setIftarAlarm()
	{
		
		iftarPos=spinnerIftar.getSelectedItemPosition();
		if(day!=0)
		{
			boolean previous=isPrevious(iftarPos, ifMinute, ifHour, false);
			if(previous)
			{
				PendingIntent pending=PendingIntent.getBroadcast(getApplicationContext(), id+31, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
				Calendar cal=getCalender(month, day, ifHour, ifMinute,false);
				manager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pending);
			}
			String ss=month+":"+day+":"+iftarTimes.get(iftarPos);
			alarmEditor.putString("iftar_one", ss);
			alarmEditor.commit();
		}
		else
		{
			boolean previous=isPrevious(iftarPos, ifMinute, ifHour, false);
			if(previous)
			{
				PendingIntent pending=PendingIntent.getBroadcast(getApplicationContext(), rojaSerial+31, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
				Calendar cal=getCalender(rojaMonth, rojaDay, ifHour, ifMinute,false);
				manager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pending);
			}
			for(int i=0;i<ids.size();i++)
			{
				int mId=Integer.valueOf(ids.get(i));
				if(mId>rojaSerial)
				{
					PendingIntent pending=PendingIntent.getBroadcast(getApplicationContext(), mId+31, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
					int aMonth;
					if(mId<=12) aMonth=6;
					else aMonth=7;
					int aDay=18+mId;
					if(aDay>30) aDay=aDay-30;
					int aHour=Integer.valueOf(ifHours.get(i));
					int aMinute=Integer.valueOf(ifMinutes.get(i));
					Calendar cal=getCalender(aMonth, aDay, aHour, aMinute,false);
					manager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pending);
				}
			}
			alarmEditor.putInt("iftar_each",iftarTimes.get(iftarPos));
			alarmEditor.commit();
		}
		
	}
	
	
	public void removeSeheriAlarm()
	{
		seheriPos=spinnerSeheri.getSelectedItemPosition();
		if(day!=0)
		{
			boolean previous=isPrevious(seheriPos, seMinute, seHour, true);
			if(previous)
			{
				PendingIntent pending=PendingIntent.getBroadcast(getApplicationContext(), id, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
				manager.cancel(pending);
			}
			alarmEditor.putString("seheri_one", "-1");
			alarmEditor.commit();
		}
		else
		{
			boolean previous=isPrevious(seheriPos, seMinute, seHour, true);
			if(previous)
			{
				PendingIntent pending=PendingIntent.getBroadcast(getApplicationContext(), rojaSerial, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
				manager.cancel(pending);
			}
			for(int i=0;i<ids.size();i++)
			{
				int mId=Integer.valueOf(ids.get(i));
				if(mId>rojaSerial)
				{
					PendingIntent pending=PendingIntent.getBroadcast(getApplicationContext(), mId, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
					manager.cancel(pending);
				}
			}
			alarmEditor.putInt("seheri_each", -1);
			alarmEditor.commit();
		}
		
	}
	
	
	public void removeIftarAlarm()
	{
		iftarPos=spinnerIftar.getSelectedItemPosition();
		if(day!=0)
		{
			boolean previous=isPrevious(iftarPos, ifMinute, ifHour, false);
			if(previous)
			{
				PendingIntent pending=PendingIntent.getBroadcast(getApplicationContext(), id+31, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
				manager.cancel(pending);
			}
			alarmEditor.putString("iftar_one", "-1");
			alarmEditor.commit();
		}
		else
		{
			boolean previous=isPrevious(iftarPos, ifMinute, ifHour, false);
			if(previous)
			{
				PendingIntent pending=PendingIntent.getBroadcast(getApplicationContext(), rojaSerial+31, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
				manager.cancel(pending);
			}
			for(int i=0;i<ids.size();i++)
			{
				int mId=Integer.valueOf(ids.get(i));
				if(mId>rojaSerial)
				{
					PendingIntent pending=PendingIntent.getBroadcast(getApplicationContext(), mId+31, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
					manager.cancel(pending);
				}
			}
			alarmEditor.putInt("iftar_each", -1);
			alarmEditor.commit();
		}
	}
	
	
	public boolean isPrevious(int aPos,int aMinute,int aHour, boolean isSeheri)
	{
		int alarmMinute,alarmHour;
		if(isSeheri) alarmMinute=aMinute-(seheriTimes.get(aPos)%60);
		else alarmMinute=aMinute-(iftarTimes.get(aPos)%60);
		if(isSeheri) alarmHour=aHour-(seheriTimes.get(aPos)/60);
		else alarmHour=aHour-(iftarTimes.get(aPos)/60);
		if(isSeheri==false) alarmHour=alarmHour+12;
		Calendar cal=Calendar.getInstance();
		int currentHour=cal.get(Calendar.HOUR_OF_DAY);
		int currentMinute=cal.get(Calendar.MINUTE);
		int currentDay=cal.get(Calendar.DAY_OF_MONTH);
		int currentMonth=cal.get(Calendar.MONTH)+1;
		
		if(currentMonth<=rojaMonth)
		{
			if(currentDay<rojaDay) return true;
			else if(currentDay==rojaDay)
			{
				if(currentHour<alarmHour) return true;
				else if(currentHour==alarmHour)
				{
					if(currentMinute<alarmMinute) return true;
				}
			}
		}
		return false;
	}
	
	
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
    	// TODO Auto-generated method stub
        if (resultCode == Activity.RESULT_OK && requestCode == 5)
        {
             Uri uri = intent.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);

             if (uri != null)
             {
            	// ringtone = RingtoneManager.getRingtone(getApplicationContext(), uri);
                 String ring = uri.toString();
                 SharedPreferences ringPref=getSharedPreferences("Ringtone", MODE_PRIVATE);
 				 SharedPreferences.Editor ringEditor=ringPref.edit();
 				 ringEditor.putString("ringtone", ring);
 				 ringEditor.commit();
 				 showToast("রিংটোন সেট করা হয়েছে");
             }
             else
             {
            	 //showToast("রিংটোন সেট করা সম্ভব হয় নি");
             }
         } 
    }
	
	
	
	
	public void vibrationClicked()
	{
		SharedPreferences vibPref=getSharedPreferences("Vivration", MODE_PRIVATE);
		SharedPreferences.Editor vibEditor=vibPref.edit();
		boolean status=vibPref.getBoolean("vibrate", false);
		if(status)
		{
			imgVibrate.setImageResource(R.drawable.checkbox_unchecked);
			vibEditor.putBoolean("vibrate", false);
			vibEditor.commit();
		}
		else
		{
			imgVibrate.setImageResource(R.drawable.checkbox_checked);
			vibEditor.putBoolean("vibrate", true);
			vibEditor.commit();
		}
	}
	
	
	
	public void showToast(String text)
	{
		LayoutInflater li = LayoutInflater.from(this);
		View view = li.inflate(R.layout.custom_toast, null);
		TextView txtToast=(TextView)view.findViewById(R.id.txtToast);
		txtToast.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			txtToast.setText(convertedText);
		}
		else
		{
			txtToast.setText(text);
		}
		Toast toast=new Toast(this);
		toast.setView(view);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.show();
	}
	
	

}
