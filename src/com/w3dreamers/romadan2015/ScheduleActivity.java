package com.w3dreamers.romadan2015;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ScheduleActivity extends Activity{
	
	
    int[] txtId={R.id.textView1,R.id.textView2,R.id.textView3,R.id.textView4,R.id.textView5,R.id.textView6,R.id.textView7,R.id.textView8,R.id.textView9,R.id.textView10,R.id.textView11,R.id.textView12,R.id.textView13,R.id.textView14,R.id.textView15,R.id.textView16,R.id.textView17,R.id.textView18,R.id.textView19,R.id.textView20,R.id.textView21,R.id.textView22,R.id.textView23,R.id.textView24,R.id.textView25,R.id.textView26,R.id.textView27,R.id.textView28,R.id.textView29,R.id.textView30,R.id.textView31,R.id.textView32,R.id.textView33,R.id.textView34,R.id.textView35,R.id.textView36,R.id.textView37,R.id.textView38,R.id.textView39,R.id.textView40,R.id.textView41,R.id.textView42};
    int[] txtIdWeek={R.id.txtSun,R.id.txtMon,R.id.txtTue,R.id.txtWed,R.id.txtThu,R.id.txtFri,R.id.txtSat};
    int[] linearId={R.id.linear1,R.id.linear2,R.id.linear3,R.id.linear4,R.id.linear5,R.id.linear6,R.id.linear7,R.id.linear8,R.id.linear9,R.id.linear10,R.id.linear11,R.id.linear12,R.id.linear13,R.id.linear14,R.id.linear15,R.id.linear16,R.id.linear17,R.id.linear18,R.id.linear19,R.id.linear20,R.id.linear21,R.id.linear22,R.id.linear23,R.id.linear24,R.id.linear25,R.id.linear26,R.id.linear27,R.id.linear28,R.id.linear29,R.id.linear30,R.id.linear31,R.id.linear32,R.id.linear33,R.id.linear34,R.id.linear35,R.id.linear36,R.id.linear37,R.id.linear38,R.id.linear39,R.id.linear40,R.id.linear41,R.id.linear42};	
	TextView[] txtCals=new TextView[45];
	TextView[] txtWeekDays=new TextView[9];
	LinearLayout[] linearCals=new LinearLayout[45];
	TextView txtDate,txtSeheri,txtSeheriTime,txtIftar,txtIftarTime,txtCurrentMonth;
	LinearLayout linearAlarm,linearOverlap,linearExtra,linearSelected,linearPrevMonth,linearNextMonth;
	
	private Calendar _calendar;
	private int month, year,selectedYear,selectedMonth,selectedDay,selectedRoja,startPos,endPos,thisMonth,thisYear;
//	private final DateFormat dateFormatter = new DateFormat();
//	private static final String dateTemplate = "MMMM yyyy";
	
	
	private ArrayList<String> list;
	ArrayList<Integer>state;
	ArrayList<Integer>isRoja;
	
	private static final int DAY_OFFSET = 1;
	private final String[] weekdays ={"রবি", "সোম", "মঙ্গল", "বুধ", "বৃহঃ", "শুক্র", "শনি"};
	final String[] banglaMonths = {"","জানুয়ারি", "ফেব্রুয়ারি", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টোবর", "নভেম্বর", "ডিসেম্বর"};
	final String[] banglaNums={"০","১","২","৩","৪","৫","৬","৭","৮","৯","১০","১১","১২","১৩","১৪","১৫","১৬","১৭","১৮","১৯","২০","২১","২২","২৩","২৪","২৫","২৬","২৭","২৮","২৯","৩০","৩১","৩২","৩৩","৩৪","৩৫","৩৬","৩৭","৩৮","৩৯","৪০","৪১","৪২","৪৩","৪৪","৪৫","৪৬","৪৭","৪৮","৪৯","৫০","৫১","৫২","৫৩","৫৪","৫৫","৫৬","৫৭","৫৮","৫৯","৬০"};
	private final String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
	private final int[] daysOfMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
	private int daysInMonth, prevMonthDays;
	private int currentDayOfMonth;
	private int currentWeekDay;
	
	boolean supported;
	Typeface banglaFont;
	
	ArrayList<String>ids=new ArrayList<String>();
	ArrayList<String>seHours=new ArrayList<String>();
	ArrayList<String>seMinutes=new ArrayList<String>();
	ArrayList<String>ifHours=new ArrayList<String>();
	ArrayList<String>ifMinutes=new ArrayList<String>();
	ArrayList<String>rojaDays=new ArrayList<String>();
	ArrayList<String>rojaMonths=new ArrayList<String>();
	int rojaYear=2015;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.schedule_layout);
		
		linearPrevMonth=(LinearLayout)findViewById(R.id.linearPrevMonth);
		linearNextMonth=(LinearLayout)findViewById(R.id.linearNextMonth);
		linearAlarm=(LinearLayout)findViewById(R.id.linearAlarm);
		linearOverlap=(LinearLayout)findViewById(R.id.linearOverlap);
		linearExtra=(LinearLayout)findViewById(R.id.linearExtra);
		txtCurrentMonth=(TextView)findViewById(R.id.txtCurrentMonth);
		txtDate=(TextView)findViewById(R.id.txtDate);
		txtSeheri=(TextView)findViewById(R.id.txtSeheri);
		txtSeheriTime=(TextView)findViewById(R.id.txtSeheriTime);
		txtIftar=(TextView)findViewById(R.id.txtIftar);
		txtIftarTime=(TextView)findViewById(R.id.txtIftarTime);
		
		for(int i=0;i<7;i++)
		{
			txtWeekDays[i]=(TextView)findViewById(txtIdWeek[i]);
		}
		for(int i=0;i<42;i++)
		{
			txtCals[i]=(TextView)findViewById(txtId[i]);
			linearCals[i]=(LinearLayout)findViewById(linearId[i]);
		}
		
		
		ids=(ArrayList<String>)getIntent().getSerializableExtra("ids");
		rojaDays=(ArrayList<String>)getIntent().getSerializableExtra("rojaDays");
		rojaMonths=(ArrayList<String>)getIntent().getSerializableExtra("rojaMonths");
		seHours=(ArrayList<String>)getIntent().getSerializableExtra("seHours");
		seMinutes=(ArrayList<String>)getIntent().getSerializableExtra("seMinutes");
		ifHours=(ArrayList<String>)getIntent().getSerializableExtra("ifHours");
		ifMinutes=(ArrayList<String>)getIntent().getSerializableExtra("ifMinutes");
		
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		
		txtCurrentMonth.setTypeface(banglaFont);
		txtDate.setTypeface(banglaFont);
		txtSeheri.setTypeface(banglaFont);
		txtSeheriTime.setTypeface(banglaFont);
		txtIftar.setTypeface(banglaFont);
		txtIftarTime.setTypeface(banglaFont);
		
		for(int i=0;i<7;i++)
		{
			txtWeekDays[i].setTypeface(banglaFont);
		}	
		for(int i=0;i<42;i++)
		{
			txtCals[i].setTypeface(banglaFont);
		}
		
		String seheri="সেহরী";
		String iftar="ইফতার";
		String wDay="";
		if(supported)
		{
			SpannableString convertedSeheri=AndroidCustomFontSupport.getCorrectedBengaliFormat(seheri, banglaFont, (float)1);
			txtSeheri.setText(convertedSeheri);
			SpannableString convertedIftar=AndroidCustomFontSupport.getCorrectedBengaliFormat(iftar, banglaFont, (float)1);
			txtIftar.setText(convertedIftar);
			for(int i=0;i<7;i++)
			{
				wDay=weekdays[i];
				SpannableString convertedWday=AndroidCustomFontSupport.getCorrectedBengaliFormat(wDay, banglaFont, (float)1);
				txtWeekDays[i].setText(convertedWday);
			}
		}
		else
		{
			txtSeheri.setText(seheri);
			txtIftar.setText(iftar);
			for(int i=0;i<7;i++)
			{
				wDay=weekdays[i];
				txtWeekDays[i].setText(wDay);
			}
		}
		
		_calendar = Calendar.getInstance(Locale.getDefault());
		month = _calendar.get(Calendar.MONTH) + 1;
		year = _calendar.get(Calendar.YEAR);
//		currentMonth.setText(dateFormatter.format(dateTemplate, _calendar.getTime()));
		int mMonth=_calendar.get(Calendar.MONTH)+1;
		int mYear=_calendar.get(Calendar.YEAR);
		String mmy=banglaMonths[mMonth]+" ";
		String sYear=String.valueOf(mYear);
		for(int i=0;i<sYear.length();i++)
		{
			String a=""+sYear.charAt(i);
			int b=Integer.valueOf(a);
			mmy+=banglaNums[b];
		}
		if(supported)
		{
			SpannableString convertedMMY=AndroidCustomFontSupport.getCorrectedBengaliFormat(mmy, banglaFont, (float)1);
			txtCurrentMonth.setText(convertedMMY);
		}
		else txtCurrentMonth.setText(mmy);
		
		selectedYear=thisYear=year;
		selectedMonth=thisMonth=month;
		printMonth(month, year);
		
		
		linearPrevMonth.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			/*	if(linearSelected!=null)
				{
					linearSelected.setBackgroundColor(Color.parseColor("#FFFFFF"));
				}  */
				if (month <= 1)
				{
					month = 12;
					year--;
				}
			    else
				{
					month--;
				}
			    setGridCellAdapterToDate(month, year);
			}
		});
		
		
		linearNextMonth.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			/*	if(linearSelected!=null)
				{
					linearSelected.setBackgroundColor(Color.parseColor("#FFFFFF"));
				}  */
				if (month > 11)
				{
					month = 1;
					year++;
				}
			    else
				{
					month++;
				}
			    setGridCellAdapterToDate(month, year);
			}
		});
		
		
		
		
		linearAlarm.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(selectedRoja<=0) showToast("প্রযোজ্য নয়");
				else
				{
					int seHour=0,seMinute=0,ifHour=0,ifMinute=0,id=0;
					for(int i=0;i<ids.size();i++)
					{
						if(ids.get(i).equals(String.valueOf(selectedRoja)))
						{
							seHour=Integer.valueOf(seHours.get(i));
							seMinute=Integer.valueOf(seMinutes.get(i));
							ifHour=Integer.valueOf(ifHours.get(i));
							ifMinute=Integer.valueOf(ifMinutes.get(i));
							id=Integer.valueOf(ids.get(i));
							break;
						}
					}
					
					Intent in=new Intent(ScheduleActivity.this,AlarmActivity.class);
					in.putExtra("day", selectedDay);
					in.putExtra("month", selectedMonth);
					in.putExtra("seHour", seHour);
					in.putExtra("seMinute", seMinute);
					in.putExtra("ifHour", ifHour);
					in.putExtra("ifMinute", ifMinute);
					in.putExtra("id", id);
					startActivity(in);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				}
			}
		});
		
		
		
	}
	
	
	
	public void showToast(String text)
	{
		LayoutInflater li = LayoutInflater.from(this);
		View view = li.inflate(R.layout.custom_toast, null);
		TextView txtToast=(TextView)view.findViewById(R.id.txtToast);
		txtToast.setTypeface(banglaFont);
		if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			txtToast.setText(convertedText);
		}
		else
		{
			txtToast.setText(text);
		}
		Toast toast=new Toast(this);
		toast.setView(view);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.show();
	}
	
	

	private void setGridCellAdapterToDate(int month, int year)
	{
		_calendar.set(year, month - 1, _calendar.get(Calendar.DAY_OF_MONTH));
		//currentMonth.setText(dateFormatter.format(dateTemplate, _calendar.getTime()));
		int mMonth=_calendar.get(Calendar.MONTH)+1;
		int mYear=_calendar.get(Calendar.YEAR);
		String mmy=banglaMonths[mMonth]+" ";
		String sYear=String.valueOf(mYear);
		for(int i=0;i<sYear.length();i++)
		{
			String a=""+sYear.charAt(i);
			int b=Integer.valueOf(a);
			mmy+=banglaNums[b];
		}
		if(supported)
		{
			SpannableString convertedMMY=AndroidCustomFontSupport.getCorrectedBengaliFormat(mmy, banglaFont, (float)1);
			txtCurrentMonth.setText(convertedMMY);
		}
		else txtCurrentMonth.setText(mmy);
		selectedMonth=thisMonth=month;
		selectedYear=thisYear=year;
		printMonth(month, year);
	}
	
	
	
	private String getMonthAsString(int i)
	{
		return months[i];
	}  
/*
    private String getWeekDayAsString(int i)
	{
		return weekdays[i];
	}  */

    private int getNumberOfDaysOfMonth(int i)
	{
		return daysOfMonth[i];
	}
    
    
    
    
    public int getCurrentDayOfMonth()
	{
		return currentDayOfMonth;
	}

    private void setCurrentDayOfMonth(int currentDayOfMonth)
	{
		this.currentDayOfMonth = currentDayOfMonth;
	}
    public void setCurrentWeekDay(int currentWeekDay)
	{
		this.currentWeekDay = currentWeekDay;
	}
  /*  public int getCurrentWeekDay()
	{
		return currentWeekDay;
	}   */
    
    
    
    
    private void printMonth(int mm, int yy)
	{
		// The number of days to leave blank at
		// the start of this month.
		int trailingSpaces = 0;
		int leadSpaces = 0;
		int daysInPrevMonth = 0;
		int prevMonth = 0;
		int prevYear = 0;
		int nextMonth = 0;
		int nextYear = 0;
		
		list=new ArrayList<String>();
		state=new ArrayList<Integer>();
		isRoja=new ArrayList<Integer>();
		
		Calendar calendar = Calendar.getInstance();
		if((mm-1)==calendar.get(Calendar.MONTH)&&yy==calendar.get(Calendar.YEAR))
		{
			setCurrentDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
		}
		else
		{
			setCurrentDayOfMonth(0);
		}
		//setCurrentWeekDay(calendar.get(Calendar.DAY_OF_WEEK));

		int currentMonth = mm - 1;
		String currentMonthName = getMonthAsString(currentMonth);
		daysInMonth = getNumberOfDaysOfMonth(currentMonth);

		// Gregorian Calendar : MINUS 1, set to FIRST OF MONTH
		GregorianCalendar cal = new GregorianCalendar(yy, currentMonth, 1);

		if (currentMonth == 11)
		{
				prevMonth = currentMonth - 1;
				daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
				nextMonth = 0;
				prevYear = yy;
				nextYear = yy + 1;
		}
		else if (currentMonth == 0)
		{
				prevMonth = 11;
				prevYear = yy - 1;
				nextYear = yy;
				daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
				nextMonth = 1;
		}
		else
		{
				prevMonth = currentMonth - 1;
				nextMonth = currentMonth + 1;
				nextYear = yy;
				prevYear = yy;
				daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
		}

		// Compute how much to leave before before the first day of the
		// month.
		// getDay() returns 0 for Sunday.
		int currentWeekDay = cal.get(Calendar.DAY_OF_WEEK) - 1;
		trailingSpaces = currentWeekDay;

		if (cal.isLeapYear(cal.get(Calendar.YEAR)) && mm == 2)
		{
			++daysInMonth;
		}

		// Trailing Month days
		for (int i = 0; i < trailingSpaces; i++)
		{
			//list.add(String.valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i) + "-GREY" + "-" + getMonthAsString(prevMonth) + "-" + prevYear);
			int mDay=(daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i;
			list.add(String.valueOf(mDay));
			state.add(0);
			if(selectedYear==rojaYear)
			{
				if((selectedMonth-1)==6&&mDay>=19)
				{
					isRoja.add((mDay-18));
				}
				else if((selectedMonth-1)==7&&mDay<=17)
				{
					isRoja.add((12+mDay));
				}
				else
				{
					isRoja.add(0);
				}
			}
			else
			{
				isRoja.add(0);
			}
		}
		
		startPos=trailingSpaces;
		endPos=startPos+daysInMonth-1;

		// Current Month Days
		for (int i = 1; i <= daysInMonth; i++)
		{
				if (i == getCurrentDayOfMonth())
				{	
					//list.add(String.valueOf(i) + "-BLUE" + "-" + getMonthAsString(currentMonth) + "-" + yy);
					list.add(String.valueOf(i));
					state.add(2);
				}
				else
				{
					//list.add(String.valueOf(i) + "-WHITE" + "-" + getMonthAsString(currentMonth) + "-" + yy);
					list.add(String.valueOf(i));
					state.add(1);
				}
				int mDay=i;
				if(selectedYear==rojaYear)
				{
					if(selectedMonth==6&&mDay>=19)
					{
						isRoja.add((mDay-18));
					}
					else if(selectedMonth==7&&mDay<=17)
					{
						isRoja.add((12+mDay));
					}
					else
					{
						isRoja.add(0);
					}
				}
				else
				{
					isRoja.add(0);
				}
		}

		// Leading Month days
		for (int i = 0; i < list.size()%7; i++)
		{
			//list.add(String.valueOf(i + 1) + "-GREY" + "-" + getMonthAsString(nextMonth) + "-" + nextYear);
			list.add(String.valueOf(i+1));
			state.add(0);
			int mDay=i+1;
			if(selectedYear==rojaYear)
			{
				if((selectedMonth+1)==6&&mDay>=19)
				{
					isRoja.add((mDay-18));
				}
				else if((selectedMonth+1)==7&&mDay<=17)
				{
					isRoja.add((12+mDay));
				}
				else
				{
					isRoja.add(0);
				}
			}
			else
			{
				isRoja.add(0);
			}
		}
		
		if(list.size()<=35)
		{
			linearExtra.setVisibility(View.INVISIBLE);
			linearOverlap.setVisibility(View.VISIBLE);
		}
		else
		{
			linearOverlap.setVisibility(View.INVISIBLE);
			linearExtra.setVisibility(View.VISIBLE);
		}
		
		linearSelected=null;
		selectedDay=-1;
		selectedRoja=0;
		for(int i=0;i<list.size();i++)
		{
			if(isRoja.get(i)>0)
			{
				linearCals[i].setBackgroundResource(R.drawable.background_gradient_green);
			}
			else
			{
				linearCals[i].setBackgroundColor(Color.parseColor("#FFFFFF"));
			}
			
			if(state.get(i)==0) txtCals[i].setTextColor(Color.parseColor("#999999"));
			else if(state.get(i)==2)
			{
				txtCals[i].setTextColor(Color.parseColor("#000000"));
				linearCals[i].setBackgroundResource(R.drawable.background_gradient_red);
				linearSelected=linearCals[i];
				selectedDay=Integer.valueOf(list.get(i));
				selectedRoja=isRoja.get(i);
			}
			else if(state.get(i)==1) txtCals[i].setTextColor(Color.parseColor("#000000"));
			
			int mDay=Integer.valueOf(list.get(i));
			String mText=banglaNums[mDay];
			if(supported)
			{
				SpannableString convertedMtext=AndroidCustomFontSupport.getCorrectedBengaliFormat(mText, banglaFont, (float)1);
				txtCals[i].setText(convertedMtext);
			}
			else
			{
				txtCals[i].setText(mText);
			}
		}
		
		setDateTime();
		
		
	/*	for(int i=list.size();i<42;i++)
			txtCals[i].setText("");
			*/
		
		
	}
	
    
    
    
    public void dateSelected(View view)
    {
    	int sId=view.getId();
    	for(int i=0;i<linearId.length;i++)
    	{
    		if(sId==linearId[i])
    		{
    			if(i<startPos) selectedMonth=thisMonth-1;
    			else if(i>endPos) selectedMonth=thisMonth+1;
    			else selectedMonth=thisMonth;
    			if(selectedMonth>12)
    			{
    				selectedMonth=selectedMonth-12;
    				selectedYear=thisYear+1;
    			}
    			else if(selectedMonth<1)
    			{
    				selectedMonth=12+selectedMonth;
    				selectedYear=thisYear-1;
    			}
    			else selectedYear=thisYear;
    			
    			if(linearSelected!=null)
    			{
    				if(selectedRoja==0) linearSelected.setBackgroundColor(Color.parseColor("#FFFFFF"));
    				else linearSelected.setBackgroundResource(R.drawable.background_gradient_green);
    			}
    			selectedDay=Integer.valueOf(list.get(i));
    			selectedRoja=isRoja.get(i);
    			linearSelected=(LinearLayout)findViewById(sId);
    			linearSelected.setBackgroundResource(R.drawable.background_gradient_red);
    			setDateTime();
    			break;
    		}
    	}
    }
    
    
    
    public void setDateTime()
    {
    	String mDate;
    	if(selectedDay!=-1) mDate=banglaMonths[selectedMonth]+" "+banglaNums[selectedDay]+", ";
    	else mDate=banglaMonths[selectedMonth]+" ";
    	String sYear=String.valueOf(selectedYear);
		for(int i=0;i<sYear.length();i++)
		{
			String a=""+sYear.charAt(i);
			int b=Integer.valueOf(a);
			mDate+=banglaNums[b];
		}
		String seheri,iftar;
		if(selectedRoja<=0)
		{
			seheri=iftar="প্রযোজ্য নয়";
		}
		else
		{
			int seHour=0,seMinute=0,ifHour=0,ifMinute=0;
			for(int i=0;i<ids.size();i++)
			{
				if(ids.get(i).equals(String.valueOf(selectedRoja)))
				{
					seHour=Integer.valueOf(seHours.get(i));
					seMinute=Integer.valueOf(seMinutes.get(i));
					ifHour=Integer.valueOf(ifHours.get(i));
					ifMinute=Integer.valueOf(ifMinutes.get(i));
					break;
				}
			}
			seheri="ভোর "+banglaNums[seHour]+"টা  "+banglaNums[seMinute]+"মি.";
			iftar="সন্ধ্যা "+banglaNums[ifHour]+"টা  "+banglaNums[ifMinute]+"মি.";
		}
		if(supported)
		{
			SpannableString convertedMdate=AndroidCustomFontSupport.getCorrectedBengaliFormat(mDate, banglaFont, (float)1);
			SpannableString convertedSeheri=AndroidCustomFontSupport.getCorrectedBengaliFormat(seheri, banglaFont, (float)1);
			SpannableString convertedIftar=AndroidCustomFontSupport.getCorrectedBengaliFormat(iftar, banglaFont, (float)1);
			txtDate.setText(convertedMdate);
			txtSeheriTime.setText(convertedSeheri);
			txtIftarTime.setText(convertedIftar);
		}
		else
		{
			txtDate.setText(mDate);
			txtSeheriTime.setText(seheri);
			txtIftarTime.setText(iftar);
		}
    }
	
	

}
