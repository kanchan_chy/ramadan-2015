package com.w3dreamers.romadan2015;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class AlarmReceiver extends BroadcastReceiver{
	
	private static final String ALARM_ACTION_NAME = "com.w3dreamers.ramadan.ALARM";

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
        // Handle the alarm broadcast
        if (ALARM_ACTION_NAME.equals(intent.getAction()))
        {
        	Intent alarmIntent = new Intent(context,AlarmPopUp.class); 
            alarmIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(alarmIntent);
        }
	}

}
