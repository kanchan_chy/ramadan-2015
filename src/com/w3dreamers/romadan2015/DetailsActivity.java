package com.w3dreamers.romadan2015;

import java.util.ArrayList;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

public class DetailsActivity extends Activity{
	
	TextView txtTitle,txtHeader,txtDetails;
	
	boolean supported;
	Typeface banglaFont;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.details_layout);
		txtTitle=(TextView)findViewById(R.id.txtTitle);
		txtHeader=(TextView)findViewById(R.id.txtHeader);
		txtDetails=(TextView)findViewById(R.id.txtDetails);
		
		String title=getIntent().getExtras().getString("title");
		String header=getIntent().getExtras().getString("header");
		String details=getIntent().getExtras().getString("details");
		
		SharedPreferences prefSupport=getSharedPreferences("BanglaLibrary", MODE_PRIVATE);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(getAssets(), "font/solaimanlipinormal.ttf");
		
		txtTitle.setTypeface(banglaFont);
		txtHeader.setTypeface(banglaFont);
		txtDetails.setTypeface(banglaFont);
		
		if(supported)
		{
			SpannableString convertedTitle=AndroidCustomFontSupport.getCorrectedBengaliFormat(title, banglaFont, (float)1);
			txtTitle.setText(convertedTitle);
			SpannableString convertedHeader=AndroidCustomFontSupport.getCorrectedBengaliFormat(header, banglaFont, (float)1);
			txtHeader.setText(convertedHeader);
			SpannableString convertedDetails=AndroidCustomFontSupport.getCorrectedBengaliFormat(details, banglaFont, (float)1);
			txtDetails.setText(convertedDetails);
		}
		else
		{
			txtTitle.setText(title);
			txtHeader.setText(header);
			txtDetails.setText(details);
		}
		
	}

}
