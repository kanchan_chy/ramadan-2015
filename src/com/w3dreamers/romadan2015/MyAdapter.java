package com.w3dreamers.romadan2015;

import java.util.ArrayList;
import java.util.List;

import com.dibosh.experiments.android.support.customfonthelper.AndroidCustomFontSupport;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MyAdapter extends BaseAdapter {
	
	LayoutInflater inFlater;
	
	Context context;
	ArrayList<String>itemList;
	
	boolean supported;
	Typeface banglaFont;
	
	
	public MyAdapter(Context context,ArrayList<String>itemList)
	{
		this.context=context;
		this.itemList=itemList;
		
		SharedPreferences prefSupport=context.getSharedPreferences("BanglaLibrary", 0);
		supported=prefSupport.getBoolean("supported", true);
		
		banglaFont=Typeface.createFromAsset(context.getAssets(), "font/solaimanlipinormal.ttf");
		
		inFlater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return itemList.size();
	}

	@Override
	public String getItem(int position) {
		// TODO Auto-generated method stub
		return itemList.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		  
		
		ViewHolder holder=null;
		if(convertView==null)
		{
			convertView=inFlater.inflate(R.layout.list_item, null);
		  
		    holder= new ViewHolder();
		    holder.txtItem=(TextView)convertView.findViewById(R.id.txtItem);
		    convertView.setTag(holder);
		}
		else
		{   
			    holder=(ViewHolder)convertView.getTag();
		}
		
		holder.txtItem.setTypeface(banglaFont);
		String text=itemList.get(position);
		if(supported)
		{
			SpannableString convertedText=AndroidCustomFontSupport.getCorrectedBengaliFormat(text,banglaFont, (float) 1);
			holder.txtItem.setText(convertedText);
		}
		else holder.txtItem.setText(text);
		
		return convertView;
	}

	
	public static class ViewHolder {
	    public TextView txtItem;
	    
	}
	

	
	

}
